﻿using System;
using Microsoft.Extensions.Configuration;

namespace ST.Dental.BLL
{
    public class AuthenticationServiceConfig : IAuthenticationServiceConfig
    {
        private readonly IConfiguration _config;

        public AuthenticationServiceConfig(IConfiguration config)
        {
            _config = config;
        }

        public Uri HostUri => new Uri(_config["baseTokenUrl:Auth0"]);
    }
}