﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace ST.Dental.Web.Data.Entities
{
    public class User : IdentityUser
    {
        public int UserProfileId { get; set; }
        [ForeignKey("UserProfileId")]
        public virtual UserProfile Profile { get; set; }
    }
}
