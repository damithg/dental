﻿using System;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using ST.Dental.BusinessObjects;

namespace ST.Dental.BLL
{
    public class TokenProvider : ITokenProvider
    {
        private readonly IAuthenticationServiceConfig _authenticationServiceConfig;
        private readonly ICertificateManager _certificateManager;

        private readonly ILogger<TokenProvider> _logger;

        public TokenProvider(ICertificateManager certificateManager, IAuthenticationServiceConfig authenticationServiceConfig)
        {
            _certificateManager = certificateManager;
            _authenticationServiceConfig = authenticationServiceConfig;
        }

        public AuthenticatedToken GetToken(string uri)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            try
            {
                var response = client.Execute<AuthenticatedToken>(request);
                if (response.IsSuccessful)
                {
                    return response.Data;
                }
            }
            catch (Exception e)
            {
            }

            return null;
        }

        public Tuple<HttpStatusCode, string> ClientKeyToUrl(string uri)
        {
            var client = new RestClient(uri)
            {
                //ClientCertificates = _certificateManager.GetClientCertificates(_authenticationServiceConfig.CertificateSerialNumber)
            };

            var request = new RestRequest(Method.GET);
            try
            {
                var response = client.Execute(request);
                if (!response.IsSuccessful)
                {
                    _logger.LogError("Failed to transform clientkey to URL", response);
                }

                var responseUrl = response.IsSuccessful ? JsonConvert.DeserializeObject<ClientKeyToUrlResponse>(response.Content)?.Url : response.Content;

                return new Tuple<HttpStatusCode, string>(response.StatusCode, responseUrl);
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public void Revoke(string uri, string authorization)
        {
            var client = new RestClient(uri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("authorization", authorization);

            try
            {
                var response = client.Execute<AuthenticatedToken>(request);
                if (!response.IsSuccessful)
                {
                    _logger.LogWarning("Failed to revoke token", response);
                }
            }
            catch (Exception e)
            {
            }
        }

        private class ClientKeyToUrlResponse
        {
            public string Url { get; set; }
        }
    }
}