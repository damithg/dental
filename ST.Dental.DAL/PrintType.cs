﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PrintType : BusinessObject<int>
    {
        private string _name;
        private int _requestCount;

        private PrintCategory _printCategory;
        private IList<PrintTemplate> _printTemplates;

        public PrintType(int Lid, string Name, int RequestCount)
        {
            this.ID = Lid;
            this.name = Name;
		    this.requestCount=RequestCount;
        }

        public PrintType() { }

        public PrintType(int Id) 
        {
            this.ID = Id;
        }

        public virtual PrintCategory printCategory
        {
            get { return this._printCategory; }
            set { this._printCategory = value; }
        }

        public virtual IList<PrintTemplate> printTemplates
        {
            get { return this._printTemplates; }
            set { this._printTemplates = value; }
        }

        public virtual string name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        public virtual int requestCount
        {
            get { return this._requestCount; }
            set { this._requestCount = value; }
        }


        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name + "|" +
                    requestCount).GetHashCode();
        }
    }	
}
