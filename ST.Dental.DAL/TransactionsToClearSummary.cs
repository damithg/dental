﻿using System;

namespace Quintus.Biz
{
    public class TransactionsToClearSummary
    {
        private int _index;
		private int _lid; 
		private string _transactionDescription;
		private string _accountType;
		private string _name;
		private string _action;
		private string _description;
		private decimal _amount;

        public TransactionsToClearSummary(int Index, int Lid, string TransactionDescription,
		    string AccountType, string Name, string Action, string Description, decimal Amount)
        {
            this._index = Index;
		    this._lid = Lid;
		    this._transactionDescription = TransactionDescription;
		    this._accountType = AccountType;
		    this._name = Name;
		    this._action = Action;
		    this._description = Description;
            this._amount = Amount;
        }

        public int index { get { return _index; } }
        public int lid { get { return _lid; } }
        public string transactionDescription { get { return _transactionDescription; } }
        public string accountType { get { return _accountType; } }
        public string name { get { return _name; } }
        public string action { get { return _action; } }
        public string description { get { return _description; } }
        public decimal amount { get { return _amount; } }        
    }
}
