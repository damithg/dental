﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quintus.Biz
{
    [Serializable]
    public struct PatientAddStruct
    {
        public int patientGroupId;
        public int practiceId;
        public int dentistId;
        public int categoryId;
        public Boolean collectRegFee;
        public int payMethodId;
        public DateTime startDate;

        public DateTime? renewalDate;
        public DateTime? nextRenewalDate;
        public DateTime feeReviewDate;
        public int numMonths;
        public string totalRegFee;
        public string amountDue;
        public string amountPaid;

        public int sortcode;
        public string accountNumber;
        public string accountName;

        public string notes;

        public Boolean duplicateBankDetails;
    }
}
