﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class BacsResponse : BusinessObject<int>
    {
        private string _code;
        private string _description;

        public BacsResponse() { }

        public virtual string code
        {
            get { return this._code; }
            set { this._code = value; }
        }

        public virtual string description
        {
            get { return this._description; }
            set { this._description = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    code + "|" +
                    description).GetHashCode();
        }
    }	
}
