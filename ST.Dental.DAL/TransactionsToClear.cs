﻿using System;

namespace Quintus.Biz
{
    public class TransactionsToClear
    {
        private int _lid;       
        private string _description;

        public TransactionsToClear(int Lid, string Description)
        {
            this._lid = Lid;
            this._description = Description;
        }

        public int lid { get { return _lid; } }
        public string description { get { return _description; } }
    }
}
