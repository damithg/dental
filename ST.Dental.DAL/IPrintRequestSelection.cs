﻿using System;

namespace Quintus.Biz
{
    public interface IPrintRequestSelection
    {
        PrintParameters printParameters { get; }
    }
}
