﻿using System;
using System.Collections.Generic;
using log4net;

namespace Quintus.Biz
{
    [Serializable]
    public class CategoryClass : BusinessObject<int>
    {
        // Define the Log4Net logger.
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IList<Category> _categories = new List<Category>();
        private IList<CategoryClass_Discount> _categoryDiscount = new List<CategoryClass_Discount>();
        private Dentist _dentist;
        private Bank _bank;

        private System.Nullable<int> @__dentistRef;
        private string _accountName;
        private string _accountNumber;
        private System.Nullable<bool> _AUDDISRecordSent;
        private System.Nullable<bool> _BACSDetailsChanged;
        private string _BACSRef;        
        private System.Nullable<bool> _collectPracticeFee;
        private System.Nullable<bool> _collectRegistrationFeeFlag;
        private System.Nullable<bool> _countsTowardsDiscount;        
        private string _description;
        private System.Nullable<bool> _doubleFirstMonth;
        private System.Nullable<decimal> _dpasFee;
        private string _insuranceCoverLevel;
        private System.Nullable<int> _paymentPlan;
        private System.Nullable<decimal> _registrationFee;
        private System.Nullable<bool> _numberActive;
        private System.Nullable<bool> _presentAtLastAUDDIS;
        private System.Nullable<decimal> _dpasRegistrationFee;
        private System.Nullable<bool> _dentistPaysPrf;
        private string _unpaidReport;

        #region Internet Toolkit Fields
        protected int vid = 1;
        protected int pvid = 0;
        protected int arrayIndex = 1;
        #endregion

        public CategoryClass() { }

        public virtual IList<Category> categories
        {
            get { return this._categories; }
            //protected set { this._categories = value; }
            set { this._categories = value; }
        }

        public virtual IList<Category> activeCategories
        {
            get 
            {
                IList<Category> cats = new List<Category>();
                foreach (Category cat in _categories)
                {
                    if (cat.CurrentPrice != null || cat.hasFuturePrice())
                        cats.Add(cat);
                }

                return cats; 
            }
        }

        public virtual IList<CategoryClass_Discount> categoryDiscount
        {
            get { return _categoryDiscount; }
            set { _categoryDiscount = value; }
        }

        public virtual Dentist dentist
        {
            get { return this._dentist; }
            set { this._dentist = value; }
        }

        public virtual Bank bank
        {
            get { return this._bank; }
            set { this._bank = value; }
        }

        public virtual System.Nullable<int> _dentistRef
        {
            get{return this.@__dentistRef;}
            set{this.@__dentistRef = value;}
        }

        public virtual string accountName
        {
            get{return this._accountName;}
            set{this._accountName = value;}
        }

        public virtual string accountNumber
        {
            get{return this._accountNumber;}
            set{this._accountNumber = value;}
        }

        public virtual System.Nullable<bool> AUDDISRecordSent
        {
            get{return this._AUDDISRecordSent;}
            set{this._AUDDISRecordSent = value;}
        }

        public virtual System.Nullable<bool> BACSDetailsChanged
        {
            get { return this._BACSDetailsChanged; }
            set{this._BACSDetailsChanged = value;}
        }

        public virtual string BACSRef
        {
            get{return this._BACSRef;}
            set{this._BACSRef = value;}
        }

        public virtual System.Nullable<bool> collectPracticeFee
        {
            get{return this._collectPracticeFee;}
            set{this._collectPracticeFee = value;}
        }

        public virtual System.Nullable<bool> collectRegistrationFeeFlag
        {
            get{return this._collectRegistrationFeeFlag;}
            set{this._collectRegistrationFeeFlag = value;}
        }

        public virtual System.Nullable<bool> countsTowardsDiscount
        {
            get{return this._countsTowardsDiscount;}
            set{this._countsTowardsDiscount = value;}
        }

        public virtual string description
        {
            get{return this._description;}
            set{this._description = value;}
        }

        public virtual System.Nullable<bool> doubleFirstMonth
        {
            get{return this._doubleFirstMonth;}
            set{this._doubleFirstMonth = value;}
        }

        public virtual System.Nullable<decimal> dpasFee
        {
            get{return this._dpasFee;}
            set{this._dpasFee = value;}
        }

        public virtual string insuranceCoverLevel
        {
            get{return this._insuranceCoverLevel;}
            set{this._insuranceCoverLevel = value;}
        }

        public virtual System.Nullable<int> paymentPlan
        {
            get{return this._paymentPlan;}
            set{this._paymentPlan = value;}
        }

        public virtual System.Nullable<decimal> registrationFee
        {
            get{return this._registrationFee;}
            set{this._registrationFee = value;}
        }

        public virtual System.Nullable<bool> numberActive
        {
            get{return this._numberActive;}
            set{this._numberActive = value;}
        }

        public virtual System.Nullable<bool> presentAtLastAUDDIS
        {
            get{return this._presentAtLastAUDDIS;}
            set{this._presentAtLastAUDDIS = value;}
        }

        public virtual System.Nullable<decimal> dpasRegistrationFee
        {
            get{return this._dpasRegistrationFee;}
            set{this._dpasRegistrationFee = value;}
        }

        public virtual System.Nullable<bool> dentistPaysPrf
        {
            get { return this._dentistPaysPrf; }
            set { this._dentistPaysPrf = value; }
        }

        public virtual string unpaidReport
        {
            get { return this._unpaidReport; }
            set { this._unpaidReport = value; }
        }

        public virtual string fullDescription
        {
            get { return this._description.ToString(); }
        }

        public virtual string numberActiveDesc
        {
            get
            {
                if (numberActive.HasValue)
                {
                    if (numberActive.Value == true)
                        return "Active";
                    else
                        return "Inactive";
                }
                else
                {
                    return "Inactive";
                }
            }
        }

        public virtual void AddCategory(Category newCategory)
        {
            if (_categories != null && !_categories.Contains(newCategory))
            {
                newCategory.categoryClass = this;
                _categories.Add(newCategory);
            }
        }

        public virtual void AddCategoryDiscount(ref CategoryClass_Discount ccDiscount)
        {
            try
            {
                int vid = 1;
                foreach (CategoryClass_Discount c in this.categoryDiscount)
                {
                    if (c.ID.vid > vid) vid = c.ID.vid;
                }
                vid += 1;

                ccDiscount.ID = new CompositeKey(this.ID, vid);
                this.categoryDiscount.Add(ccDiscount);
            }
            catch (Exception ex)
            {
                log.Error("Error in CategoryClass.AddCategoryDiscount", ex);
                throw ex;
            }
        }
        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    _dentistRef.ToString() + "|" +
                    _BACSRef + "|" +
                    description).GetHashCode();
        }
    }	
}
