﻿using System;

namespace Quintus.Biz
{

    public class BacsPaymentSummary
    {
        private int _account;
        private string _accountType;
        private string _name;
        private string _debitOrCredit;
        private decimal _amount;


        public BacsPaymentSummary(int Account, string AccountType, 
            string Name, string DebitOrCredit, decimal Amount)
        {
            this._account = Account;
            this._accountType = AccountType;
            this._name = Name;
            this._debitOrCredit = DebitOrCredit;
            this._amount = Amount;
        }

        public int account { get { return _account; } }
        public string accountType { get { return _accountType; } }
        public string name { get { return _name; } }
        public string debitOrCredit { get { return _debitOrCredit; } }
        public decimal amount { get { return _amount; } }
    }
}
