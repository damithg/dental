﻿using System;

namespace Quintus.Biz
{
    [Serializable]
    public class Patient_Category : BusinessObject<CompositeKey>, ICheckTransient
    {
        private Patient _patient;
        private Category _category;

        private int? _additionalDiscount;
        private bool? _collectRegistrationFee;
        private int? _currentDiscount;
        private DateTime? _endDate;
        private DateTime? _startDate;

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        #region ICheckTransient implementation

            private Boolean _isTransient = true;

            public virtual Boolean isTransient
            {
                get { return this._isTransient; }
                set { this._isTransient = value; }
            }

        #endregion

        public Patient_Category(){ }

        #region Properties

        public virtual Patient patient
        {
            get { return this._patient; }
            set { this._patient = value; }
        }

        public virtual Category category
        {
            get { return this._category; }
            set { this._category = value; }
        }

        public virtual int? additionalDiscount
        {
            get{return this._additionalDiscount;}
            set{this._additionalDiscount = value;}
        }

        public virtual bool? collectRegistrationFee
        {
            get{return this._collectRegistrationFee;}
            set{this._collectRegistrationFee = value;}
        }

        public virtual int? currentDiscount
        {
            get{return this._currentDiscount;}
            set{this._currentDiscount = value;}
        }

        public virtual DateTime? endDate
        {
            get{return this._endDate;}
            set{this._endDate = value;}
        }

        public virtual DateTime? startDate
        {
            get{return this._startDate;}
            set{this._startDate = value;}
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                additionalDiscount + "|" +
                collectRegistrationFee + "|" +
                currentDiscount + "|" +
                endDate + "|" +
                startDate + "|" +
                patient._patientReference.ToString()).GetHashCode();
        }
    }	
}
