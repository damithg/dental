﻿using System.Security.Cryptography.X509Certificates;

namespace ST.Dental.BLL
{
    public interface ICertificateManager
    {
        X509CertificateCollection GetClientCertificates(string certificateSerialNumber);
    }
}