﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class JobHistory : BusinessObject<int>
    {
        private AspnetUser _user;
        private string _jobName;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _outcome;
        private bool _warningOverridden;

        public JobHistory() { }

        public virtual AspnetUser user
        {
            get { return this._user; }
            set { this._user = value; }
        }

        public virtual string jobName
        {
            get { return this._jobName; }
            set { this._jobName = value; }
        }

        public virtual DateTime startDate
        {
            get { return this._startDate; }
            set { this._startDate = value; }
        }

        public virtual DateTime endDate
        {
            get { return this._endDate; }
            set { this._endDate = value; }
        }

        public virtual string outcome
        {
            get { return this._outcome; }
            set { this._outcome = value; }
        }

        public virtual bool warningOverridden
        {
            get { return this._warningOverridden; }
            set { this._warningOverridden = value; }
        }

        public virtual TimeSpan duration
        {
            get 
            {
                if (endDate == DateTime.MinValue ||
                    startDate == DateTime.MinValue ||
                    startDate >= endDate)
                {
                    return new TimeSpan(0);
                }
                else
                {
                    return endDate.Subtract(startDate);
                }
            }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    jobName + "|" +
                    startDate.ToString() + "|" +
                    endDate.ToString() + "|" +
                    outcome + "|" +
                    warningOverridden.ToString()).GetHashCode();
        }
    }	
}
