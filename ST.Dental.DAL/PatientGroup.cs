﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class PatientGroup : BusinessObject<int>
    {
        //private Mandate _mandate;

        private Bank _bank;
        private Practice _practice;
        private PaymentMethod _paymentMethod;
        private BacsResponse _bacsResponse;

        private IList<Patient> _patients = new List<Patient>();
        private IList<PatientGroup_MandateHistory> _mandates = new List<PatientGroup_MandateHistory>();
        private IList<BacsHistory> _bacsHistory = new List<BacsHistory>();

        private string @__uniqueId;
        private string _additionalNotes;        
        private bool? _bacsTransferUnsuccessful;
        private string _accountName;
        private string _accountNumber;
        private bool? _AUDDISRecordSent;
        private bool? _BACSDetailsChanged;
        private string _BACSRef;
        private DateTime? _dateMandateReceived;
        private bool? _dpasToCollect;
        private bool? _mandateReceived;
        private string _name;
        private DateTime? _renewalDate;
        private DateTime? _nextRenewalDate;
        private bool? _welcomeLetterSent;
        private decimal? _lastBACSFile;
        private bool? _presentAtLastAUDDIS;
        private bool? _welcomeLetterPrinted;

        public PatientGroup(){ }

        #region Internet Toolkit Fields
            protected int arrayIndex = 1;
            protected int vid = 1;
            protected int? pvid = null;
        #endregion

        #region Properties
            
            public virtual Bank bank
            {
                get { return this._bank; }
                set { this._bank = value; }
            }

            public virtual Practice practice
            {
                get { return this._practice; }
                set { this._practice = value; }
            }

            public virtual PaymentMethod paymentMethod
            {
                get { return this._paymentMethod; }
                set { this._paymentMethod = value; }
            }

            public virtual BacsResponse bacsResponse
            {
                get { return this._bacsResponse; }
                set { this._bacsResponse = value; }
            }

            public virtual IList<Patient> patients
            {
                get { return this._patients; }
                protected set { this._patients = value; }
            }

            public virtual IList<PatientGroup_MandateHistory> mandates
            {
                get { return this._mandates; }
                protected set { this._mandates = value; }
            }

            public virtual IList<BacsHistory> bacsHistory
            {
                get { return this._bacsHistory; }
                protected set { this._bacsHistory = value; }
            }
        
            public virtual Patient HeadOfGroup
            {
                get
                {
                    foreach (Patient patient in patients)
                    {
                        if (patient.isGroupPayer.HasValue && patient.isGroupPayer.Value == true)
                        {
                            return patient;
                        }
                    }
                    return null;
                }
            }

            public virtual string _uniqueId
            {
                get{return this.@__uniqueId;}
                set{this.@__uniqueId = value;}
            }

            public virtual string additionalNotes
            {
                get{return this._additionalNotes;}
                set{this._additionalNotes = value;}
            }

            public virtual System.Nullable<bool> bacsTransferUnsuccessful
            {
                get { return this._bacsTransferUnsuccessful; }
                set { this._bacsTransferUnsuccessful = value; }
            }

            public virtual string accountName
            {
                get{return this._accountName;}
                set{this._accountName = value;}
            }

            public virtual string accountNumber
            {
                get{return this._accountNumber;}
                set{this._accountNumber = value;}
            }

            public virtual System.Nullable<bool> AUDDISRecordSent
            {
                get{return this._AUDDISRecordSent;}
                set{this._AUDDISRecordSent = value;}
            }

            public virtual System.Nullable<bool> BACSDetailsChanged
            {
                get{return this._BACSDetailsChanged;}
                set{this._BACSDetailsChanged = value;}
            }

            public virtual string BACSRef
            {
                get{return this._BACSRef;}
                set{this._BACSRef = value;}
            }

            public virtual System.Nullable<System.DateTime> dateMandateReceived
            {
                get{return this._dateMandateReceived;}
                set{this._dateMandateReceived = value;}
            }

            public virtual System.Nullable<bool> dpasToCollect
            {
                get{return this._dpasToCollect;}
                set{this._dpasToCollect = value;}
            }

            public virtual System.Nullable<bool> mandateReceived
            {
                get {return this._mandateReceived;}
                set{this._mandateReceived = value;}
            }

            public virtual string name
            {
                get{return this._name;}
                set{this._name = value;}
            }

            public virtual System.Nullable<System.DateTime> renewalDate
            {
                get{return this._renewalDate;}
                set{this._renewalDate = value;}
            }

            public virtual System.Nullable<System.DateTime> nextRenewalDate
            {
                get { return this._nextRenewalDate; }
                set { this._nextRenewalDate = value; }
            }

            public virtual System.Nullable<bool> welcomeLetterSent
            {
                get{return this._welcomeLetterSent;}
                set{this._welcomeLetterSent = value;}
            }

            public virtual System.Nullable<decimal> lastBACSFile
            {
                get{return this._lastBACSFile;}
                set{this._lastBACSFile = value;}
            }

            public virtual System.Nullable<bool> presentAtLastAUDDIS
            {
                get{return this._presentAtLastAUDDIS;}
                set{this._presentAtLastAUDDIS = value;}
            }

            public virtual System.Nullable<bool> welcomeLetterPrinted
            {
                get{return this._welcomeLetterPrinted;}
                set{this._welcomeLetterPrinted = value;}
            }

            public virtual IList<Patient> activePatients
            {
                get 
                {
                    IList<Patient> ap = new List<Patient>();
                    foreach (Patient p in patients)
                    {
                        if (p.deRegistrationDate.HasValue == false || p.deRegistrationDate.Value > DateTime.Now)
                        {
                            ap.Add(p);
                        }
                    }

                    return ap; 
                }
            }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    @__uniqueId + "|" +
                    BACSRef).GetHashCode();
        }
    }	
}
