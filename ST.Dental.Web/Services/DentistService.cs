﻿using System.Collections.Generic;
using ST.Dental.Web.Data.Entities;

namespace ST.Dental.Web.Services
{
    public class DentistService : IDentistService
    {
        public IEnumerable<Dentist> GetDentists(int practiceId)
        {
            var dentists = new List<Dentist>
            {
                new Dentist() {FirstName = "Dr Jay"}
            };

            return dentists;
        }
    }
}