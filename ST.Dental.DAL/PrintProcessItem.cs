﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PrintProcessItem : BusinessObject<int>
    {
        private PrintTemplate _printTemplate;

        private string _processName;
        private string _displayName;

        public PrintProcessItem() { }

        public virtual PrintTemplate printTemplate
        {
            get { return this._printTemplate; }
            set { this._printTemplate = value; }
        }

        public virtual string processName
        {
            get { return this._processName; }
            set { this._processName = value; }
        }

        public virtual string displayName
        {
            get { return this._displayName; }
            set { this._displayName = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    processName + "|" +
                    displayName).GetHashCode();
        }
    }	
}
