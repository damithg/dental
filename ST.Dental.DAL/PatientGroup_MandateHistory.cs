﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class PatientGroup_MandateHistory : BusinessObject<CompositeKey>, ICheckTransient
    {
        private PatientGroup _patientGroup;
        private Image _image;

        private System.Nullable<bool> _currentMandate;
        private System.Nullable<DateTime> _dateMandateReceived;

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        #region ICheckTransient implementation

            private Boolean _isTransient = true;

            public virtual Boolean isTransient
            {
                get { return this._isTransient; }
                set { this._isTransient = value; }
            }

        #endregion

        public PatientGroup_MandateHistory() { }

        #region Properties

        public virtual PatientGroup patientGroup
        {
            get { return this._patientGroup; }
            set { this._patientGroup = value; }
        }

        public virtual Image image
        {
            get { return this._image; }
            set { this._image = value; }
        }

        public virtual System.Nullable<bool> currentMandate
        {
            get { return this._currentMandate; }
            set { this._currentMandate = value; }
        }

        public virtual System.Nullable<DateTime> dateMandateReceived
        {
            get { return this._dateMandateReceived; }
            set { this._dateMandateReceived = value; }
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                currentMandate.ToString() + "|" +
                dateMandateReceived.ToString()).GetHashCode();
        }
    }	
}
