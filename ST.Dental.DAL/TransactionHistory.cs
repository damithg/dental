﻿using System;

namespace Quintus.Biz
{
    /// <summary>
    /// Encapsulates the transaction history for a particular patient.  Note that this isn't a 
    /// DomainObject, just a POCO value object.  Furthermore, since it's a value
    /// object, it's immutable after construction.
    /// </summary>
    public class TransactionHistory
    {
        private int _lid;
        private DateTime _date;
        private decimal _totalTaken;
        private string _accountNo;
        private string _sortCode;
        private string _bacsResponse;
        private int _transactionId;
        private DateTime _effectiveDate;
        private string _patientName;
        private decimal _amount;
        private string _description;
        private string _action;
        private string _debitCredit;
        private string _type;
        private string _narrative;
        private DateTime _transactionDate;
        private string _settlementMethod;


        public TransactionHistory(int Lid, DateTime Date, decimal TotalTaken, string AccountNo, 
            string SortCode, string BacsResponse, int TransactionId, DateTime EffectiveDate, 
            string PatientName, decimal Amount, string Description, string Action, string DebitCredit, 
            string Type, string Narrative, DateTime TransactionDate, string SettlementMethod)
        {
            this._lid = Lid;
            this._date = Date;
            this._totalTaken = TotalTaken;
            this._accountNo = AccountNo;
            this._sortCode = SortCode;
            this._bacsResponse = BacsResponse;
            this._transactionId = TransactionId;
            this._effectiveDate = EffectiveDate;
            this._patientName = PatientName;
            this._amount = Amount;
            this._description = Description;
            this._action = Action;
            this._debitCredit = DebitCredit;
            this._type = Type;
            this._narrative = Narrative;
            this._transactionDate = TransactionDate;
            this._settlementMethod = SettlementMethod;
        }

        public int Lid { get { return _lid; } }
        public DateTime Date { get { return _date; } }
        public decimal TotalTaken { get { return _totalTaken; } }
        public string AccountNo { get { return _accountNo; } }
        public string SortCode { get { return _sortCode; } }
        public string BacsResponse { get { return _bacsResponse; } }
        public int TransactionId { get { return _transactionId; } }
        public DateTime effectiveDate { get { return _effectiveDate; } }
        public string PatientName { get { return _patientName; } }
        public decimal amount { get { return _amount; } }
        public string description { get { return _description; } }
        public string Action { get { return _action; } }
        public string DebitCredit { get { return _debitCredit; } }
        public string Type { get { return _type; } }
        public string narrative { get { return _narrative; } }
        public DateTime TransactionDate { get { return _transactionDate; } }
        public string SettlementMethod { get { return _settlementMethod; } }
    }
}
