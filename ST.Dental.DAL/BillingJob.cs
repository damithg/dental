﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class BillingJob : BusinessObject<string>
    {
        private IList<BillingJob> _predecessors = new List<BillingJob>();

        private string _description;
        private DateTime _lastRunDate;
        private string _outcome;
        private int _jobOrder;
        private int? _earliestDay;
        private bool _running;

        public BillingJob() 
        {
            _running = false;
        }

        public enum Status
        {
            Green,
            Amber,
            Red,
            Blue
        }

        public virtual IList<BillingJob> predecessors
        {
            get { return this._predecessors; }
            protected set { this._predecessors = value; }
        }

        public virtual string description
        {
            get { return this._description; }
            set { this._description = value; }
        }

        public virtual DateTime lastRunDate
        {
            get { return this._lastRunDate; }
            set { this._lastRunDate = value; }
        }

        public virtual string outcome
        {
            get { return this._outcome; }
            set { this._outcome = value; }
        }

        public virtual int jobOrder
        {
            get { return this._jobOrder; }
            set { this._jobOrder = value; }
        }

        public virtual int? earliestDay
        {
            get { return this._earliestDay; }
            set { this._earliestDay = value; }
        }

        public virtual bool running
        {
            get { return this._running; }
            set { this._running = value; }
        }

        public virtual string buttonText
        {
            get
            {
                if (status == Status.Blue) return "View Job";
                else return "Run Job";
            }
        }

        /// <summary>
        /// Determine whether this job should be run.
        /// </summary>
        public virtual Status status
        {
            get
            {
                if (_running) return Status.Blue;
                if (_earliestDay > DateTime.Now.Day) return Status.Red;

                bool isAmber = false;
                foreach (BillingJob predecessor in predecessors)
                {
                    if ((predecessor.lastRunDate < this.lastRunDate || 
                        predecessor.outcome != "Success") &&
                        predecessor.running == false)
                    {
                        return Status.Red;
                    }
                    if (predecessor.running)
                    {
                        isAmber = true;
                    }
                }
                if (isAmber) return Status.Amber;
                else return Status.Green;
            }
        }

        public virtual bool outOfOrder
        {
            get
            {
                foreach (BillingJob predecessor in predecessors)
                {
                    if (predecessor.lastRunDate < this.lastRunDate ||
                        predecessor.outcome != "Success")
                    {
                        return true;
                    }
                }
                return false;
            }
        }


        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    description).GetHashCode();
        }
    }	
}
