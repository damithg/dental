﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class BacsHistory : BusinessObject<int>
    {
        private BacsResponse _bacsResponse;
        private PatientGroup _patientGroup;

        private string _accountNumber;
        private string _accountSortcode;
        private string _bacsReference;
        private decimal _totalTaken;
        private DateTime _datePosted;

        #region Internet Toolkit Fields
        protected int vid = 1;
        protected int pvid = 0;
        protected int arrayIndex = 1;
        #endregion

        public BacsHistory() { }

        public virtual BacsResponse bacsResponse
        {
            get { return this._bacsResponse; }
            set { this._bacsResponse = value; }
        }

        public virtual PatientGroup patientGroup
        {
            get { return this._patientGroup; }
            set { this._patientGroup = value; }
        }

        public virtual string accountNumber
        {
            get { return this._accountNumber; }
            set { this._accountNumber = value; }
        }

        public virtual string accountSortcode
        {
            get { return this._accountSortcode; }
            set { this._accountSortcode = value; }
        }

        public virtual string bacsReference
        {
            get { return this._bacsReference; }
            set { this._bacsReference = value; }
        }

        public virtual decimal totalTaken
        {
            get { return this._totalTaken; }
            set { this._totalTaken = value; }
        }

        public virtual DateTime datePosted
        {
            get { return this._datePosted; }
            set { this._datePosted = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    bacsReference + "|" +
                    totalTaken.ToString() + "|" +
                    datePosted.ToShortDateString()).GetHashCode();
        }
    }	
}
