﻿using System;

namespace Quintus.Biz
{
    [Serializable]
    public struct PatientStruct
    {
        public int localeInstanceId;
        public string BacsRef;
        public int DentistRef;
        public int PracticeRef;
        public int PatientRef;
        public string Firstname;
        public string Surname;
        public string Address1;
        public string TownCity;
        public string Postcode;
        public DateTime Dob;
        public string CompetitorRef;
        public string BankAccount;
        public bool includeCancelled;
    }
}
