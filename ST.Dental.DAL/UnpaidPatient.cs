﻿using System;

namespace Quintus.Biz
{
    public class UnpaidPatient
    {
        private int _practiceRef;
        private string _officeContact;
        private string _bacsRef;
        private string _unpaidReport;
        private int _patientId;

        public UnpaidPatient(int PracticeRef, string OfficeContact,
            string BacsRef, string UnpaidReport, int PatientId)
        {
            this._practiceRef = PracticeRef;
            this._officeContact = OfficeContact;
            this._bacsRef = BacsRef;
            this._unpaidReport = UnpaidReport;
            this._patientId = PatientId;
        }

        public int practiceRef { get { return _practiceRef; } }
        public string officeContact { get { return _officeContact; } }
        public string bacsRef { get { return _bacsRef; } }
        public string unpaidReport { get { return _unpaidReport; } }
        public int patientId { get { return _patientId; } }
    }
}
