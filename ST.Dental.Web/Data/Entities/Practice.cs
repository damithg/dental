﻿using System;
using System.Text;

namespace ST.Dental.Web.Data.Entities
{
    [Serializable]
    public class Practice
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }	
}