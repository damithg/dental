﻿using System;

namespace Quintus.Biz
{
    public class DpasPlPatient
    {
        private Guid _oid;
        private string _title;
        private string _forename;
        private string _surname;
        private string _head;
        private string _groupRef;
        private string _address1;
        private string _address2;
        private string _address3;
        private string _address4;
        private string _postcode;
        private string _denplanRef;
        private string _category;
        private int _dentist;
        private DateTime _dob;
        private int @__patientRef;
        private int _barcode;
        private int _practiceRef;
        private string _telephoneNumber;
        private DateTime _loadedTimestamp;
        private System.Nullable<DateTime> _scannedTimestamp;
        private System.Nullable<DateTime> _pushedTimestamp;
        private Guid _scanner;
        private string _competitor;
        private int _groupSize;
        private bool _loadPatient = true;

        public DpasPlPatient(Guid Oid, string Title, string Forename, string Surname, 
            string Head, string GroupRef, string Address1, string Address2, string Address3, 
            string Address4, string Postcode, string DenplanRef, string Category, 
            int Dentist, DateTime Dob, int PatientRef, int Barcode, int PracticeRef, 
            string TelephoneNumber, DateTime LoadedTimestamp, DateTime ScannedTimestamp,
            DateTime PushedTimestamp, Guid Scanner, string Competitor, int GroupSize)
        {
            this._oid = Oid;
            this._title = Title;
            this._forename = Forename;
            this._surname = Surname;
            this._head = Head;
            this._groupRef = GroupRef;
            this._address1 = Address1;
            this._address2 = Address2;
            this._address3 = Address3;
            this._address4 = Address4;
            this._postcode = Postcode;
            this._denplanRef = DenplanRef;
            this._category = Category;
            this._dentist = Dentist;
            this._dob = Dob;
            this.@__patientRef = PatientRef;
            this._barcode = Barcode;
            this._practiceRef = PracticeRef;
            this._telephoneNumber = TelephoneNumber;
            this._loadedTimestamp = LoadedTimestamp;
            this._scannedTimestamp = ScannedTimestamp;
            this._pushedTimestamp = PushedTimestamp;
            this._scanner = Scanner;
            this._competitor = Competitor;
            this._groupSize = GroupSize;
        }

        public Guid oid { get { return _oid; } }

        public string title 
        { 
            get { return _title; }
            set { this._title = value; }
        }

        public string forename 
        {
            get { return _forename; }
            set { this._forename = value; }
        }

        public string surname 
        {
            get { return _surname; }
            set { this._surname = value; }
        }

        public string head 
        { 
            get 
            {
                if (_head == "Y")
                    return "Y";
                else
                    return "N";
            }
            set { this._head = value; }
        }

        public bool isHead
        {
            get
            {
                if (_head == "Y")
                    return true;
                else
                    return false;
            }
            set 
            {
                if (value == true)
                    _head = "Y";
                else
                    _head = "N";
            }
        }

        public string groupRef 
        {
            get { return _groupRef; }
            set { this._groupRef = value; }
        }

        public string address1 
        {
            get { return _address1; }
            set { this._address1 = value; }
        }

        public string address2 
        {
            get { return _address2; }
            set { this._address2 = value; }
        }

        public string address3 
        {
            get { return _address3; }
            set { this._address3 = value; }
        }

        public string address4 
        {
            get { return _address4; }
            set { this._address4 = value; }
        }

        public string postcode 
        {
            get { return _postcode; }
            set { this._postcode = value; }
        }

        public string denplanRef 
        {
            get { return _denplanRef; }
            set { this._denplanRef = value; }
        }

        public string category 
        {
            get { return _category; }
            set { this._category = value; }
        }

        public int dentist 
        {
            get { return _dentist; }
            set { this._dentist = value; }
        }

        public DateTime dob 
        {
            get { return _dob; }
            set { this._dob = value; }
        }

        public int _patientRef 
        {
            get { return @__patientRef; }
            set { this.@__patientRef = value; }
        }

        public int barcode { get { return _barcode; } }

        public int practiceRef 
        {
            get { return _practiceRef; }
            set { this._practiceRef = value; }
        }

        public string telephoneNumber { get { return _telephoneNumber; } }

        public DateTime loadedTimestamp { get { return _loadedTimestamp; } }

        public System.Nullable<DateTime> scannedTimestamp 
        {
            get { return _scannedTimestamp; }
            set { this._scannedTimestamp = value; } 
        }

        public System.Nullable<DateTime> pushedTimestamp
        {
            get { return _pushedTimestamp; }
            set { this._pushedTimestamp = value; }
        }

        public Guid scanner { get { return _scanner; } }

        public string competitor 
        {
            get { return _competitor; }
            set { this._competitor = value; }
        }

        public int groupSize { get { return _groupSize; } }

        public string name
        {
            get { return this.title + " " + this.forename + " " + this.surname; }
        }

        public bool loadPatient
        {
            get { return _loadPatient; }
            set { this._loadPatient = value; }
        }

    }
}
