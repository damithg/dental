﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Practice_PriceHistory : BusinessObject<CompositeKey>, ICheckTransient
    {
        private Practice _practice;

        private System.Nullable<decimal> _fee;
        private System.Nullable<System.DateTime> _liveFrom;
        private System.Nullable<System.DateTime> _liveUntil;
        
        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        #region ICheckTransient Members
        private Boolean _isTransient = true;
        public virtual Boolean isTransient
        {
            get { return this._isTransient; }
            set { this._isTransient = value; }
        }

        #endregion

        public Practice_PriceHistory(){ }

        #region Properties

        public virtual Practice practice
        {
            get { return this._practice; }
            set { this._practice = value; }
        }

        public virtual System.Nullable<decimal> fee
        {
            get{return this._fee;}
            set{this._fee = value;}
        }

        public virtual System.Nullable<System.DateTime> liveFrom
        {
            get{return this._liveFrom;}
            set{this._liveFrom = value;}
        }

        public virtual System.Nullable<System.DateTime> liveUntil
        {
            get{return this._liveUntil;}
            set{this._liveUntil = value;}
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    fee + "|" +
                    liveFrom + "|" +
                    liveUntil + "|" +
                    practice._practiceReference.ToString()).GetHashCode();
        }
    }	
}
