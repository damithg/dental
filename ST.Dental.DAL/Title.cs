﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Title : BusinessObject<int>
    {
        private string _title;

        public Title() { }

        public virtual string title
        {
            get { return this._title; }
            set { this._title = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    title).GetHashCode();
        }
    }	
}
