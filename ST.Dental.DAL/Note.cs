﻿using System;

namespace ST.Dental.DAL
{
    [Serializable]
    public class Note
    {
        private AspnetUser _user;
        private System.Action _action;

        private DateTime _creationDate;
        private string _subject;
        private string _body;

        public Note() { }

        public virtual AspnetUser user
        {
            get { return this._user; }
            set { this._user = value; }
        }

        public virtual System.Action action
        {
            get { return this._action; }
            set { this._action = value; }
        }

        public virtual DateTime creationDate
        {
            get { return this._creationDate; }
            set { this._creationDate = value; }
        }

        public virtual string subject
        {
            get { return this._subject; }
            set { this._subject = value; }
        }

        public string Body { get; set; }
    }
}