﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Image : BusinessObject<int>
    {
        private string _fileName;
        private string _fileName_URL;
        private string _name;

        public Image() { }

        public virtual string fileName
        {
            get { return this._fileName; }
            set { this._fileName = value; }
        }

        public virtual string fileName_URL
        {
            get { return this._fileName_URL; }
            set { this._fileName_URL = value; }
        }

        public virtual string name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    fileName + "|" +
                    fileName_URL + "|" +
                    name).GetHashCode();
        }
    }	
}
