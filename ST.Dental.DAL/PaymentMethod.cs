﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PaymentMethod : BusinessObject<int>
    {
        private string _name;

        public PaymentMethod() { }

        public virtual string name
        {
            get{return this._name;}
            set{this._name = value;}
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name).GetHashCode();
        }
    }	
}
