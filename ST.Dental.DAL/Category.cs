﻿using System;
using System.Collections.Generic;
using log4net;

namespace Quintus.Biz
{
    [Serializable]
    public class Category : BusinessObject<int>
    {
        // Define the Log4Net logger.
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IList<Category_PriceHistory> _priceHistory = new List<Category_PriceHistory>();
        private CategoryClass _categoryClass;
        private string _name;

        #region Internet Toolkit Fields
        protected int vid = 1;
        protected int pvid = 0;
        protected int arrayIndex = 1;
        #endregion

        public Category(){}

        public virtual string name
        {
            get{return this._name;}
            set{this._name = value;}
        }

        public virtual IList<Category_PriceHistory> priceHistory
        {
            get { return _priceHistory; }
            protected set { _priceHistory = value; }
        }
        public virtual CategoryClass categoryClass
        {
            get { return this._categoryClass; }
            set { this._categoryClass = value; }
        }

        public virtual string fullDescription
        {
            get
            {
                if (CurrentPrice == null)
                {
                    return this.name;
                }
                else
                {
                    return this.name + string.Format(" - {0:C}", CurrentPrice.fee);
                }
            }
        }

        #region Category_PriceHistory functionality

        public virtual void AddPriceHistoryItem(ref Category_PriceHistory HistoryItem)
        {
            try
            {
                int vid = 1;
                foreach (Category_PriceHistory ph in this.priceHistory)
                {
                    if (ph.ID.vid > vid) vid = ph.ID.vid;
                }
                vid += 1;

                HistoryItem.ID = new CompositeKey(this.ID, vid);
                this.priceHistory.Add(HistoryItem);
            }
            catch (Exception ex)
            {
                log.Error("Error in Patient.AddPatientCategory", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Find the first price that starts before today and ends after today.
        /// If none found return null.
        /// </summary>
        public virtual Category_PriceHistory CurrentPrice
        {
            get
            {
                Category_PriceHistory foundPrice = null;

                if (priceHistory.Count == 0)
                {
                    foundPrice = null;
                }
                else
                {
                    //Find the first price that starts before today and ends after today.
                    //If none found return null.
                    foreach (Category_PriceHistory price in priceHistory)
                    {
                        if (price.liveFrom <= DateTime.Now && (price.liveUntil == null | price.liveUntil == DateTime.MinValue | price.liveUntil > DateTime.Now))
                        {
                            foundPrice = price;
                            break;
                        }
                    }
                }

                return foundPrice;
            }
        }

        public virtual bool hasFuturePrice()
        {
            bool result = false;

            if (priceHistory.Count == 0)
            {
                result = false;
            }
            else
            {
                //Find the first price that starts after today.
                foreach (Category_PriceHistory price in priceHistory)
                {
                    if (price.liveFrom >= DateTime.Now && (price.liveUntil == null | price.liveUntil == DateTime.MinValue | price.liveUntil > price.liveFrom))
                    {
                        result = true;
                        break;
                    }
                }
            }

            return result;
        }


        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name).GetHashCode();
        }
    }	
}
