﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class AspnetRole : BusinessObject<Guid>
    {
        private Guid _applicationId;
        private string _roleName;
        private string _loweredRoleName;
        private string _description;

        public AspnetRole() { }

        public AspnetRole(Guid ApplicationId, Guid RoleId, string RoleName, string LoweredRoleName, string Description) 
        {
            applicationId = ApplicationId;
            base.ID = RoleId;
            roleName = RoleName;
            loweredRoleName = LoweredRoleName;
            description = Description;
        }

        #region Properties

        protected virtual Guid applicationId
        {
            get { return this._applicationId; }
            set { this._applicationId = value; }
        }

        public virtual string roleName
        {
            get { return this._roleName; }
            set { this._roleName = value; }
        }

        public virtual string loweredRoleName
        {
            get { return this._loweredRoleName; }
            set { this._loweredRoleName = value.ToLower(); }
        }

        public virtual string description
        {
            get { return this._description; }
            set { this._description = value; }
        }

        #endregion

        public AspnetRole(string RoleName) 
        {
            this.roleName = RoleName;
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    roleName).GetHashCode();
        }
		
    }
}
