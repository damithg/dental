﻿namespace ST.Dental.Web.Data
{
    public interface IDentistRepository
    {
        bool SaveAll();
        void AddEntity(object model);
    }
}