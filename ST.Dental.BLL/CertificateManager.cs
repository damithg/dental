﻿using System.Security.Cryptography.X509Certificates;

namespace ST.Dental.BLL
{
    public class CertificateManager : ICertificateManager
    {
        public X509CertificateCollection GetClientCertificates(string certificateSerialNumber)
        {
            using (var store = new X509Store(StoreName.Root, StoreLocation.LocalMachine))
            {
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                var certificate = store.Certificates.Find(X509FindType.FindBySerialNumber, certificateSerialNumber, true);
                return certificate;
            }
        }
    }
}