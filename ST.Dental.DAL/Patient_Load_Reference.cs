﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class Patient_Load_Reference : BusinessObject<int>
    {
        private Patient _patient;
        
        private Int64 _patientReference;
        private bool _mandateExceptionFlag;

        public Patient_Load_Reference() { }

        #region Properties

        public virtual Patient patient
        {
            get { return this._patient; }
            set { this._patient = value; }
        }

        public virtual Int64 patientReference
        {
            get { return this._patientReference; }
            set { this._patientReference = value; }
        }

        public virtual bool mandateExceptionFlag
        {
            get { return this._mandateExceptionFlag; }
            set { this._mandateExceptionFlag = value; }
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                patientReference.ToString() + "|" +
                mandateExceptionFlag.ToString()).GetHashCode();
        }
    }	
}
