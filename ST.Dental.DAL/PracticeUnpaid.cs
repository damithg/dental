﻿using System;

namespace Quintus.Biz
{

    public class PracticeUnpaid
    {
       private int? @__practiceReference;
       private string _practiceName;
       private int _patientId; 
       private string _firstname;
       private string _surname;
       private string _addr1;
       private string _postcode;
       private DateTime _dateOfBirth;
       private string _bacsRef;
       private DateTime _datePosted;
       private Decimal _amount;
       private string _reason;


       public PracticeUnpaid(int? PracticeReference, string PracticeName,
            int PatientId, string Firstname, string Surname, string Addr1,
            string Postcode, DateTime DateOfBirth, string BacsRef, DateTime DatePosted,
            Decimal Amount, string Reason)
        {
            this.@__practiceReference = PracticeReference;
            this._practiceName = PracticeName;
            this._patientId = PatientId; 
            this._firstname = Firstname;
            this._surname = Surname;
            this._addr1 = Addr1;
            this._postcode = Postcode;
            this._dateOfBirth = DateOfBirth;
            this._bacsRef = BacsRef;
            this._datePosted = DatePosted;
            this._amount = Amount;
            this._reason = Reason;
        }

       public int? practiceReference { get { return @__practiceReference; } }
       public string practiceName { get { return _practiceName; } }
       public int patientId { get { return _patientId; } }
       public string firstname { get { return _firstname; } }
       public string surname { get { return _surname; } }
       public string addr1 { get { return _addr1; } }
       public string postcode { get { return _postcode; } }
       public DateTime dateOfBirth { get { return _dateOfBirth; } }
       public string bacsRef { get { return _bacsRef; } }
       public DateTime datePosted { get { return _datePosted; } }
       public Decimal amount { get { return _amount; } }
       public string reason { get { return _reason; } }
    }
}
