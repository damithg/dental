﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using log4net;

namespace Quintus.Biz
{
    [Serializable]
    public class Patient : BusinessObject<int>
    {
        // Define the Log4Net logger.
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private PatientGroup _patientGroup;
        private Dentist _primaryDentist;
        private Patient_History _patient_History;
        private Patient_Load_Reference _patientLoadReference;
        private Gender _gender;
        private IList<Note> _notes = new List<Note>();
        private IList<Patient_Category> _patient_Categories = new List<Patient_Category>();
        
        private string _addr1;
        private string _addr2;
        private string _addr3;
        private string _county;
        private string _postcode;
        private string _townCity;
        private string _firstName;
        private string _lastName;
        private string _middleInitials;
        private string _salutation;
        private string _title;
        private int? @__patientReference;
        private int? _account;
        private DateTime? _dateOfBirth;
        private DateTime? _deRegistrationDate;
        private bool? _isGroupPayer;
        private int? _practiceReference;
        private DateTime? _registrationDate;
        private string _fax;
        private string _landLine;
        private int? _dfId;
        private int _percentageDiscount;
        private DateTime? _datePrfPaid;
        private bool? _collectPRF;
        private string _membershipStatus;

        public Patient(){ }

        #region Internet Toolkit Fields
            protected int arrayIndex = 1;
            protected int vid = 1;
            protected int? pvid = null;
        #endregion

        #region Properties

        public virtual PatientGroup patientGroup
        {
            get { return this._patientGroup; }
            set{this._patientGroup = value;}
        }

        public virtual Dentist primaryDentist
        {
            get { return this._primaryDentist; }
            set { this._primaryDentist = value; }
        }

        public virtual Patient_History patient_History
        {
            get { return this._patient_History; }
            protected set { this._patient_History = value; }
        }

        public virtual Patient_Load_Reference patientLoadReference
        {
            get { return this._patientLoadReference; }
            set { this._patientLoadReference = value; }
        }

        public virtual Gender gender
        {
            get { return this._gender; }
            set { this._gender = value; }
        }

        public virtual IList<Note> notes
        {
            get { return this._notes; }
            protected set { this._notes = value; }
        }

        public virtual IList<Patient_Category> patient_Categories
        {
            get { return this._patient_Categories; }
            protected set { this._patient_Categories = value; }
        }

        public virtual string addr1
        {
            get{return this._addr1;}
            set{this._addr1 = value;}
        }

        public virtual string addr2
        {
            get{return this._addr2;}
            set{this._addr2 = value;}
        }

        public virtual string addr3
        {
            get{return this._addr3;}
            set{this._addr3 = value;}
        }

        public virtual string county
        {
            get{return this._county;}
            set{this._county = value;}
        }

        public virtual string postcode
        {
            get{return this._postcode;}
            set{this._postcode = value;}
        }

        public virtual string townCity
        {
            get{return this._townCity;}
            set{this._townCity = value;}
        }

        public virtual string firstName
        {
            get{return this._firstName;}
            set{this._firstName = value;}
        }

        public virtual string lastName
        {
            get{return this._lastName;}
            set{this._lastName = value;}
        }

        public virtual string middleInitials
        {
            get{return this._middleInitials;}
            set{this._middleInitials = value;}
        }

        // Salutaion will be always constructed using title, firstname, lastname
        // DB column salutation can be made redundant
        public virtual string salutation
        {
            get
            {
                string sal;
                if (string.IsNullOrEmpty(_title))
                {
                    sal = firstName + " " + lastName;
                }
                else
                {
                    sal = title + " " + firstName + " " + lastName; 
                }
                return sal;
            }
            set {_salutation = value;}
        }

        public virtual string title
        {
            get{return this._title;}
            set{this._title = value;}
        }

        public virtual int? _patientReference
        {
            get{return this.@__patientReference;}
            set{this.@__patientReference = value;}
        }

        public virtual int? account
        {
            get{return this._account;}
            set { this._account = value; }
        }

        public virtual DateTime? dateOfBirth
        {
            get{return this._dateOfBirth;}
            set{this._dateOfBirth = value;}
        }

        public virtual DateTime? deRegistrationDate
        {
            get{return this._deRegistrationDate;}
            set{this._deRegistrationDate = value;}
        }

        public virtual bool? isGroupPayer
        {
            get{return this._isGroupPayer;}
            set{this._isGroupPayer = value;}
        }

        public virtual string GroupPayerDescription
        {
            get 
            {
                if (this.isGroupPayer == true)
                    return "Yes";
                else
                    return "No";
            }
        }

        public virtual int? practiceReference
        {
            get{return this._practiceReference;}
            set{this._practiceReference = value;}
        }

        public virtual DateTime? registrationDate
        {
            get{return this._registrationDate;}
            set{this._registrationDate = value;}
        }

        public virtual string fax
        {
            get{return this._fax;}
            set{this._fax = value;}
        }

        public virtual string landLine
        {
            get{return this._landLine;}
            set{this._landLine = value;}
        }

        public virtual int? dfId
        {
            get{return this._dfId;}
            set{this._dfId = value;}
        }

        public virtual string name
        {
            get { return this.title + " " + this.firstName + " " + this.lastName; }
        }

        public virtual string fullDescription
        {
            get
            {
                return this._patientReference.ToString() + " - " + this.name;
            }
        }

        public virtual string status
        {
            get
            {
                if (this.deRegistrationDate.HasValue && this.deRegistrationDate <= DateTime.Now)
                {
                    return "Cancelled - " + this.deRegistrationDate.Value.ToShortDateString();
                }
                else if (this.deRegistrationDate.HasValue && this.deRegistrationDate > DateTime.Now)
                {
                    return "Cancelling on " + this.deRegistrationDate.Value.ToShortDateString();
                }
                else if (this.CurrentCategory != null && this.CurrentCategory.startDate <= DateTime.Now)
                {
                    return "Live";
                }
                else
                {
                    return "Pending";
                }
            }
        }

        public virtual string cancelled
        {
            get
            {
                if (this.deRegistrationDate.HasValue && this.deRegistrationDate <= DateTime.Now)
                {
                    return "Cancelled";
                }
                else
                {
                    return null;
                }
            }
        }

        public virtual int percentageDiscount
        {
            get { return this._percentageDiscount; }
            protected set { this._percentageDiscount = value; }
        }

        public virtual DateTime? datePrfPaid
        {
            get { return this._datePrfPaid; }
            set { this._datePrfPaid = value; }
        }

        public virtual bool? collectPRF
        {
            get { return this._collectPRF; }
            set { this._collectPRF = value; }
        }

        public virtual string membershipStatus
        {
            get { return _membershipStatus; }
            set { _membershipStatus = value; }
        }

        public virtual decimal discountedFee
        {
            get 
            {
                if (this.CurrentCategory == null)
                    return 0;

                decimal price = 0;
                decimal additionalDiscount = 0;
                decimal returnVal = 0;

                Category category = this.CurrentCategory.category;
                if (category != null)
                {
                    Category_PriceHistory priceHistory = category.CurrentPrice;
                    if (priceHistory != null && priceHistory.fee.HasValue)
                        price = (decimal)priceHistory.fee.Value;
                }

                if (this.CurrentCategory.additionalDiscount.HasValue)
                    additionalDiscount = (decimal)this.CurrentCategory.additionalDiscount.Value;

                returnVal = (((decimal)100 - (decimal)this.percentageDiscount - additionalDiscount) / (decimal)100) * price;

                return returnVal; 
            }
        }

        public virtual string formattedAddress
        {
            get
            {
                StringBuilder address = new StringBuilder();

                address.AppendLine(addr1);
                if (!string.IsNullOrEmpty(addr2))
                    address.AppendLine(addr2);
                if (!string.IsNullOrEmpty(addr3))
                    address.AppendLine(addr3);
                if (!string.IsNullOrEmpty(townCity))
                    address.AppendLine(townCity);
                if (!string.IsNullOrEmpty(county))
                    address.AppendLine(county);
                if (!string.IsNullOrEmpty(postcode))
                    address.AppendLine(postcode);

                return address.ToString();
            }
        }

        public virtual bool HasGroupPayer
        {
            get
            {
                if (isGroupPayer.HasValue && isGroupPayer.Value == true)
                {
                    return true;
                }
                else if (patientGroup.HeadOfGroup == null)
                {
                    return false;
                }
                else if (patientGroup.HeadOfGroup.registrationDate.HasValue &&
                    patientGroup.HeadOfGroup.registrationDate < DateTime.Now &&
                    (patientGroup.HeadOfGroup.deRegistrationDate.HasValue == false ||
                     patientGroup.HeadOfGroup.deRegistrationDate > DateTime.Now))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public virtual string mergeFieldAddress
        {
            get
            {
                StringBuilder address = new StringBuilder();

                address.Append(addr1);
                if (!string.IsNullOrEmpty(addr2))
                {
                    address.Append("\r\n");
                    address.Append(addr2);
                }
                if (!string.IsNullOrEmpty(addr3))
                {
                    address.Append("\r\n");
                    address.Append(addr3);
                }
                if (!string.IsNullOrEmpty(townCity))
                {
                    address.Append("\r\n");
                    address.Append(townCity);
                }
                if (!string.IsNullOrEmpty(county))
                {
                    address.Append("\r\n");
                    address.Append(county);
                }
                if (!string.IsNullOrEmpty(postcode))
                {
                    address.Append("\r\n");
                    address.Append(postcode);
                }

                return address.ToString();
            }
        }

        #endregion

        #region Patient Category Functionality

        public virtual void AddPatientCategory(ref Patient_Category patientCategory)
        {
            try
            {
                //If we are due to collect a registration fee on the current rate, 
                //collect it on the new rate instead.
                if (this.CurrentCategory != null)
                {
                    if (this.CurrentCategory.collectRegistrationFee.HasValue && this.CurrentCategory.collectRegistrationFee.Value == true)
                    {
                        patientCategory.collectRegistrationFee = true;
                    }
                }

                //Close off any existing categories
                foreach (Patient_Category pc in patient_Categories)
                {
                    if (pc.endDate == null || pc.endDate > patientCategory.startDate)
                        pc.endDate = patientCategory.startDate;
                }

                //Add the new category
                int vid = 0;
                foreach (Patient_Category pc in patient_Categories)
                {
                    if (pc.ID.vid > vid) vid = pc.ID.vid;
                }
                vid += 1;
                //patientCategory.a
                //this.patient_Categories.
                patientCategory.ID = new CompositeKey(this.ID, vid);
                this.patient_Categories.Add(patientCategory);
               
            }
            catch (Exception ex)
            {
                Log.Error("Error in Patient.AddPatientCategory", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Close off any existing categories
        /// </summary>
        /// <param name="EndDate"></param>
        public virtual void EndPatientCategories(DateTime EndDate)
        {
            foreach (Patient_Category pc in patient_Categories)
            {
                if (pc.endDate == null || pc.endDate > EndDate)
                    pc.endDate = EndDate;
            }
        }

        public virtual Patient_Category CurrentCategory
        {
            get
            {
                Patient_Category foundCategory = null;

                if (patient_Categories.Count == 0)
                {
                    foundCategory = null;
                }
                else
                {
                    //Find the first category that starts before today and ends after today
                    foreach (Patient_Category pc in patient_Categories)
                    {
                        if (pc.startDate <= DateTime.Now &&
                            (pc.endDate.HasValue == false ||
                            pc.endDate > DateTime.Now))
                        {
                            foundCategory = pc;
                        }
                    }

                    //No active categories found, so just return the latest category
                    if (foundCategory == null)
                        foundCategory = patient_Categories[patient_Categories.Count - 1];
                }

                return foundCategory;
            }
        }

        #endregion

        /// <summary>
        /// Add a new note to the notes collection.
        /// </summary>
        /// <returns>Returns the id of the note.</returns>
        public virtual void AddNote(ref Note newNote)
        {
            try
            {
                this.notes.Add(newNote);
            }
            catch (Exception ex)
            {
                Log.Error("Error in Patient.AddNote", ex);
                throw ex;
            }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    firstName + "|" +
                    lastName + "|" +
                    title + "|" +
                    postcode + "|" +
                    dateOfBirth).GetHashCode();
        }
    }	
}
