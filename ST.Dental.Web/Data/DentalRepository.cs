﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using ST.Dental.Web.Data.Entities;

namespace ST.Dental.Web.Data
{
    public class DentalRepository : IDentistRepository
    {
        private readonly DentalContext _context;
        private readonly ILogger<DentalRepository> _logger;

        public DentalRepository(Web.Data.DentalContext context, ILogger<DentalRepository> logger)
        {
            _context = context;
            _logger = logger;
        }

        public IEnumerable<Dentist> GetAllDentists()
        {
            return _context.Dentists;
        }

        public void AddEntity(object model)
        {
            _context.Add(model);
        }

        public bool SaveAll()
        {
            return _context.SaveChanges() > 0;
        }
    }
}
