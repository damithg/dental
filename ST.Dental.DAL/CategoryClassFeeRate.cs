﻿using System;

namespace Quintus.Biz
{
    public class CategoryClassFeeRate
    {
        private string _feeRate;
        private string _status;
        private decimal _fee;
        private DateTime _liveFrom;
        private System.Nullable<DateTime> _liveUntil;


        public CategoryClassFeeRate(string FeeRate, string Status, decimal Fee,
            DateTime LiveFrom, System.Nullable<DateTime> LiveUntil)
        {
            this._feeRate = FeeRate;
            this._status = Status;
            this._fee = Fee;
            this._liveFrom = LiveFrom;
            this._liveUntil = LiveUntil;
        }

        public string feeRate { get { return _feeRate; } }
        public string status { get { return _status; } }
        public decimal fee { get { return _fee; } }
        public DateTime liveFrom { get { return _liveFrom; } }
        public System.Nullable<DateTime> liveUntil { get { return _liveUntil; } }
    }
}
