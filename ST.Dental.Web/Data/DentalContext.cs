﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using ST.Dental.Web.Data.Entities;

namespace ST.Dental.Web.Data
{
    public class DentalContext : IdentityDbContext<User>
    {
        public DentalContext(DbContextOptions<DentalContext> options) : base(options)
        {
        }

        public DbSet<Practice> Practices { get; set; }
        public DbSet<Dentist> Dentists { get; set; }
    }

    //public class ApplicationContextDbFactory : IDesignTimeDbContextFactory<DentalContext>
    //{
    //    DentalContext IDesignTimeDbContextFactory<DentalContext>.CreateDbContext(string[] args)
    //    {
    //        var optionsBuilder = new DbContextOptionsBuilder<DentalContext>();
    //        optionsBuilder.UseSqlServer<DentalContext>("");

    //        return new DentalContext(optionsBuilder.Options);
    //    }
    //}
}
