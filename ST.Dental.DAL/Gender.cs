﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Gender : BusinessObject<int>
    {
        private string _gender;

        public Gender() { }

        public virtual string gender
        {
            get { return this._gender; }
            set { this._gender = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    gender).GetHashCode();
        }
    }	
}
