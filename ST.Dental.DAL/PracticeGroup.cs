﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class PracticeGroup : BusinessObject<int>
    {
        private IList<Practice> _practices = new List<Practice>(); 
        
        private System.Nullable<int> @__groupReference;		
		private string _name;

        #region Internet Toolkit Fields
            protected int arrayIndex = 1;
            protected int vid = 1;
            protected int? pvid = null;
        #endregion

        public PracticeGroup(){}

        public virtual IList<Practice> practices
        {
            get { return this._practices; }
            protected set { this._practices = value; }
        }

        public virtual System.Nullable<int> _groupReference
		{
			get{return this.@__groupReference;}
			set{this.@__groupReference = value;}
		}

        public virtual string name
		{
			get{return this._name;}
			set{this._name = value;}
		}
  
        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    _groupReference.ToString() + "|" +
                    name).GetHashCode();
        }
    }	
}
