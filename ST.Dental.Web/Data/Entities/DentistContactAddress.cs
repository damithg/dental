﻿using System;
using System.Text;

namespace ST.Dental.Web.Data.Entities
{
    [Serializable]
    public class DentistContactAddress 
    {
        public DentistContactAddress() { }

        public int Id { get; set; }
        public Dentist Dentist { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string TownCity { get; set; }

        

        public string FormattedAddress
        {
            get
            {
                StringBuilder address = new StringBuilder();
                address.AppendFormat(" {0}\n", Address1);
                if (!string.IsNullOrEmpty(Address2))
                    address.AppendFormat(" {0}\n", Address2);
                if (!string.IsNullOrEmpty(Address3))
                    address.AppendFormat(" {0}\n", Address3);
                if (!string.IsNullOrEmpty(TownCity))
                    address.AppendFormat(" {0}\n", TownCity);
                if (!string.IsNullOrEmpty(County))
                    address.AppendFormat(" {0}\n", County);
                if (!string.IsNullOrEmpty(Postcode))
                    address.AppendFormat(" {0}\n", Postcode);

                return address.ToString();
            }
        }
    }	
}
