﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class LedgerTransaction : BusinessObject<int>
    {
        private IList<LedgerTransactionEntries> _transactionEntries = new List<LedgerTransactionEntries>();
        
        private System.Nullable<bool> _amended;
        private System.Nullable<System.DateTime> _datePosted;
        private System.Nullable<System.DateTime> _dateTime;
        private string _description;
        private System.Nullable<System.DateTime> _effectiveDate;
        private System.Nullable<int> _ledger;
        private string _narrative;
        private System.Nullable<int> _originalTransaction;
        private System.Nullable<int> _transactionType;

        public LedgerTransaction() { }

        public virtual IList<LedgerTransactionEntries> transactionEntries
        {
            get { return _transactionEntries; }
            protected set { _transactionEntries = value; }
        }

        public virtual System.Nullable<bool> amended
        {
            get{return this._amended;}
            set{this._amended = value;}
        }

        public virtual System.Nullable<System.DateTime> datePosted
        {
            get{return this._datePosted;}
            set{this._datePosted = value;}
        }

        public virtual System.Nullable<System.DateTime> dateTime
        {
            get{return this._dateTime;}
            set{this._dateTime = value;}
        }

        public virtual string description
        {
            get{return this._description;}
            set{this._description = value;}
        }

        public virtual System.Nullable<System.DateTime> effectiveDate
        {
            get{return this._effectiveDate;}
            set{this._effectiveDate = value;}
        }

        public virtual System.Nullable<int> ledger
        {
            get{return this._ledger;}
            set{this._ledger = value;}
        }

        public virtual string narrative
        {
            get{return this._narrative;}
            set{this._narrative = value;}
        }

        public virtual System.Nullable<int> originalTransaction
        {
            get{return this._originalTransaction;}
            set{this._originalTransaction = value;}
        }

        public virtual System.Nullable<int> transactionType
        {
            get{return this._transactionType;}
            set{this._transactionType = value;}
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    _effectiveDate.ToString() + "|" +
                    _description).GetHashCode();
        }
    }	
}
