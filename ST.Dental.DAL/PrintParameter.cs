﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PrintParameter : BusinessObject<int>
    {
        private string _name;
        private string _paramValue;

        private PrintRequest _printRequest;

        public PrintParameter() { }

        public PrintParameter(string Name, string Value) 
        {
            name = Name;
            paramValue = Value;
        }

        public virtual PrintRequest printRequest
        {
            get { return this._printRequest; }
            set { this._printRequest = value; }
        }

        public virtual string name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual string paramValue
        {
            get { return _paramValue; }
            set { _paramValue = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name + "|" +
                    paramValue).GetHashCode();
        }
    }	
}
