﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Practice_PhoneNumber : BusinessObject<CompositeKey>, ICheckTransient
    {
        private Practice _practice;
        
        private string _description;
        private string _number;

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        public Practice_PhoneNumber(){ }

        #region Properties

        public virtual Practice practice
        {
            get { return this._practice; }
            set { this._practice = value; }
        }

        public virtual string description
        {
            get{return this._description;}
            set{this._description = value;}
        }

        public virtual string number
        {
            get{return this._number;}
            set{this._number = value;}
        }

        #endregion

        #region ICheckTransient implementation

        private Boolean _isTransient = true;

        public virtual Boolean isTransient
        {
            get { return this._isTransient; }
            set { this._isTransient = value; }
        }

        #endregion


        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    description + "|" +
                    number + "|" +
                    practice._practiceReference.ToString()).GetHashCode();
        }
    }	
}
