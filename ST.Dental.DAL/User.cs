﻿using System;

namespace ST.Dental.DAL
{
    [Serializable]
    public class User 
    {
        public string UserName { get; set; }
        public DateTime? LastActivityDate { get; set; }
        
        public User() { }
    }
}
