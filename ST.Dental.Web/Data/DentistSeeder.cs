﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using ST.Dental.Web.Data.Entities;

namespace ST.Dental.Web.Data
{
    public class DentistSeeder
    {
        private readonly DentalContext _context;
        private readonly IHostingEnvironment _hosting;
        private readonly UserManager<User> _userManager;

        public DentistSeeder(DentalContext context,
            IHostingEnvironment hosting,
            UserManager<User> userManager)
        {
            _context = context;
            _hosting = hosting;
            _userManager = userManager;
        }

        public async Task Seed()
        {
            _context.Database.EnsureCreated();

            var user = await _userManager.FindByEmailAsync("damith@test.com");

            if (user == null)
            {
                user = new User()
                {
                    UserName = "damith@test.com",
                    Email = "damith@test.com",
                    Profile = new UserProfile
                    {
                        FirstName = "Damith",
                        LastName = "Gunawardana",
                    }
                };

                var result = await _userManager.CreateAsync(user, "P@ssw0rd!");
                if (result != IdentityResult.Success)
                {
                    throw new InvalidOperationException("Failed to create default user");
                }
            }
        }
    }
}
