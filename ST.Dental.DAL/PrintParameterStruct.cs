﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class PrintParameters
    {
        public string Status;
        public int CategoryId;
        public int TypeId;
        public IList<String> CSAs;
        public IList<Int32> PracticeRefs;
        public int printJobId;
        public bool hasValue;
        public DateTime? requestedAfter;
        public DateTime? requestedBefore;

        public PrintParameters()
        {
            CSAs = new List<String>();
            PracticeRefs = new List<Int32>();
        }
    }
}
