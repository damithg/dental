﻿namespace Quintus.Biz
{
    public class MembershipDataPatient
    {
        public string Title { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string TownCity { get; set; }
        public string County { get; set; }
        public string Postcode { get; set; }
        public string BacsRef { get; set; }
        public string InsuranceCoverLevel { get; set; }
        public double InsuranceAmount { get; set; }
        public int PracticeRef { get; set; }

        public MembershipDataPatient(string title, string firstname, string lastname, string address1, string address2, string address3, 
            string townCity, string county, string postcode, string bacsRef, string insuranceCoverLevel, double insuranceAmount, int practiceRef )
        {
            Title = title;
            Firstname = firstname;
            Lastname = lastname;
            Address1 = address1;
            Address2 = address2;
            Address3 = address3;
            TownCity = townCity;
            County = county;
            Postcode = postcode;
            BacsRef = bacsRef;
            InsuranceCoverLevel = insuranceCoverLevel;
            InsuranceAmount = insuranceAmount;
            PracticeRef = practiceRef ; 
        }

        public string Name
        {
            get { return Title + " " + Firstname + " " + Lastname; }
        }
    }
}
