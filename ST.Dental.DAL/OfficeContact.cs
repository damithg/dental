﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class OfficeContact : BusinessObject<int>
    {
        private string _firstName;
        private string _lastName; 
        private string _middleInitials;
        private string _salutation;
        private string _title;
        private int _isActive;

        public OfficeContact() { }

        #region Internet Toolkit Fields
            protected int arrayIndex = 1;
            protected int vid = 1;
            protected int? pvid = null;
        #endregion

        public virtual string firstName
        {
            get { return this._firstName; }
            set { this._firstName = value; }
        }

        public virtual string lastName
        {
            get { return this._lastName; }
            set { this._lastName = value; }
        }

        public virtual string middleInitials
        {
            get { return this._middleInitials; }
            set { this._middleInitials = value; }
        }

        public virtual string salutation
        {
            get { return this._salutation; }
            set { this._salutation = value; }
        }

        public virtual string title
        {
            get { return this._title; }
            set { this._title = value; }
        }

        public virtual string name
        {
            get { return this.firstName + " " + this.lastName; }
        }

        public virtual int isActive
        {
            get { return this._isActive; }
            set { this._isActive = value; }
        }

        public virtual string shortName
        {
            get 
            {
                if (this.firstName.Length > 0)
                {
                    return this.firstName.Substring(0, 1) + " " + this.lastName;
                }
                else
                {
                    return this.lastName;
                }
            }
        }
        
        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name).GetHashCode();
        }
    }	
}
