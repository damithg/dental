﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PrintTemplate : BusinessObject<int>
    {
        private string _name;
        private string _templatePath;
        private string _templateName;
        private string _dataMethodName;

        private PrintTechnology _printTechnology;
        private IList<PrintRequest> _printRequests;

        public PrintTemplate() { }

        public PrintTemplate(int Id) 
        {
            this.ID = Id;
        }

        public virtual PrintTechnology printTechnology
        {
            get { return this._printTechnology; }
            set { this._printTechnology = value; }
        }

        public virtual IList<PrintRequest> printRequests
        {
            get { return this._printRequests; }
            set { this._printRequests = value; }
        }

        public virtual string name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        public virtual string templatePath
        {
            get { return this._templatePath; }
            set { this._templatePath = value; }
        }

        public virtual string templateName
        {
            get { return this._templateName; }
            set { this._templateName = value; }
        }

        public virtual string dataMethodName
        {
            get { return this._dataMethodName; }
            set { this._dataMethodName = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name + "|" +
                    templatePath + "|" +
                    templateName).GetHashCode();
        }
    }	
}
