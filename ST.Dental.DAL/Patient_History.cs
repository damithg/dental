﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Patient_History : BusinessObject<int>
    {
        private Patient _patient;
        private Competitor _competitor;
        private Image _cancellationImage;

        private System.Nullable<bool> _cancellationReceived;
        private string _competitorReference;
        private System.Nullable<System.DateTime> _dateToldOfCancellation;
        private System.Nullable<int> _mailing;
        private System.Nullable<System.DateTime> _dateCancellationReceived;

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        public Patient_History() { }

        #region Properties

        public virtual Patient patient
        {
            get { return this._patient; }
            set { this._patient = value; }
        }

        public virtual Competitor competitor
        {
            get { return this._competitor; }
            set { this._competitor = value; }
        }

        public virtual Image cancellationImage
        {
            get{return this._cancellationImage;}
            set{this._cancellationImage = value;}
        }

        public virtual System.Nullable<bool> cancellationReceived
        {
            get{return this._cancellationReceived;}
            set{this._cancellationReceived = value;}
        }

        public virtual string competitorReference
        {
            get{return this._competitorReference;}
            set{this._competitorReference = value;}
        }

        public virtual System.Nullable<System.DateTime> dateToldOfCancellation
        {
            get{return this._dateToldOfCancellation;}
            set{this._dateToldOfCancellation = value;}
        }

        public virtual System.Nullable<int> mailing
        {
            get{return this._mailing;}
            set{this._mailing = value;}
        }

        public virtual System.Nullable<System.DateTime> dateCancellationReceived
        {
            get { return this._dateCancellationReceived; }
            set { this._dateCancellationReceived = value; }
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    cancellationImage + "|" +
                    cancellationReceived + "|" +
                    competitorReference + "|" +
                    dateToldOfCancellation.ToString() + "|" +
                    mailing + "|" +
                    dateCancellationReceived.ToString() + "|" +
                    patient._patientReference.ToString()).GetHashCode();
        }
    }	
}
