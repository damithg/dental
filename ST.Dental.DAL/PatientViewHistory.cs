﻿using System;

namespace Quintus.Biz
{
    [Serializable]
    public class PatientViewHistory : BusinessObject<int>
    {
        private int _patientId;       
        private string _patientName;
        private string _bacsRef;
        private DateTime _dateOfBirth;
        private string _postcode;
        private string _groupPayerDescription;
        private string _patientStatus;
        private DateTime _searchDate;
        private string _userName;

        public PatientViewHistory(){ }

        //public PatientViewHistory(int PatientId, string Name, string BacsRef, 
        //    DateTime DateOfBirth, string Postcode, string GroupPayerDescription,
        //    string Status, DateTime SearchDate)
        //{
        //    this._patientId = PatientId;
        //    this._name = Name;
        //    this._bacsRef = BacsRef;
        //    this._dateOfBirth = DateOfBirth;
        //    this._postcode = Postcode;
        //    this._groupPayerDescription = GroupPayerDescription;
        //    this._status = Status;
        //    this._searchDate = SearchDate;
        //}

        public virtual int patientId 
        { 
            get { return this._patientId; }
            set { this._patientId = value; }
        }

        public virtual string patientName 
        {
            get { return this._patientName; }
            set { this._patientName = value; }
        }

        public virtual string bacsRef 
        {
            get { return this._bacsRef; }
            set { this._bacsRef = value; }
        }

        public virtual DateTime dateOfBirth 
        {
            get { return this._dateOfBirth; }
            set { this._dateOfBirth = value; }
        }

        public virtual string postcode 
        {
            get { return this._postcode; }
            set { this._postcode = value; }
        }

        public virtual string groupPayerDescription 
        {
            get { return this._groupPayerDescription; }
            set { this._groupPayerDescription = value; }
        }

        public virtual string patientStatus 
        {
            get { return this._patientStatus; }
            set { this._patientStatus = value; }
        }

        public virtual DateTime searchDate 
        {
            get { return this._searchDate; }
            set { this._searchDate = value; }
        }

        public virtual string userName
        {
            get { return this._userName; }
            set { this._userName = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    patientId.ToString() + "|" +
                    patientName + "|" +
                    bacsRef + "|" +
                    dateOfBirth + "|" +
                    postcode + "|" +
                    groupPayerDescription + "|" +
                    patientStatus + "|" +
                    searchDate.ToString() + "|" +
                    userName).GetHashCode();
        }
    }
}


