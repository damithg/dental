﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Bank : BusinessObject<int>
    {
        private string _branch;
        private string _name;
        private string _fax;
        private string _landLine;
        private System.Nullable<int> _sortcode;

        #region Internet Toolkit Fields
        protected int vid = 1;
        protected int pvid = 0;
        protected int arrayIndex = 1;
        #endregion

        public Bank(){}

        public virtual string branch
        {
            get{return this._branch;}
            set{this._branch = value;}
        }

        public virtual string name
        {
            get{return this._name;}
            set{this._name = value;}
        }

        public virtual string fax
        {
            get{return this._fax;}
            set{this._fax = value;}
        }

        public virtual string landLine
        {
            get{return this._landLine;}
            set{this._landLine = value;}
        }

        public virtual System.Nullable<int> sortcode
        {
            get{return this._sortcode;}
            set{this._sortcode = value;}
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name + "|" +
                    sortcode).GetHashCode();
        }
    }	
}
