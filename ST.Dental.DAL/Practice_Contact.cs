﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Practice_Contact : BusinessObject<CompositeKey>, ICheckTransient
    {
        private Practice _practice;

        private string _firstName;
        private string _lastName;
        private string _middleInitials;
        private string _salutation;
        private string _title;

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        #region ICheckTransient implementation

        private Boolean _isTransient = true;

        public virtual Boolean isTransient
        {
            get { return this._isTransient; }
            set { this._isTransient = value; }
        }

        #endregion

        public Practice_Contact(){ }

        #region Properties

        public virtual Practice practice
        {
            get { return this._practice; }
            set { this._practice = value; }
        }

        public virtual string firstName
        {
            get { return this._firstName; }
            set { this._firstName = value; }
        }

        public virtual string lastName
        {
            get{return this._lastName;}
            set{this._lastName = value;}
        }

        public virtual string middleInitials
        {
            get{return this._middleInitials;}
            set{this._middleInitials = value;}
        }

        public virtual string salutation
        {
            get{return this._salutation;}
            set{this._salutation = value;}
        }

        public virtual string title
        {
            get{return this._title;}
            set{this._title = value;}
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    firstName + "|" +
                    lastName + "|" +
                    middleInitials + "|" +
                    salutation + "|" +
                    title + "|" +
                    practice._practiceReference).GetHashCode();
        }

    }	
}
