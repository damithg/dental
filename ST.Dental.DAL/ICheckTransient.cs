﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Quintus.Biz
{
    public interface ICheckTransient
    {
        Boolean isTransient { get; set; }
    }
}
