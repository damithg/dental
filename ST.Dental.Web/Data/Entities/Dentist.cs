﻿using System;

namespace ST.Dental.Web.Data.Entities
{
    [Serializable]
    public class Dentist 
    {
        public Dentist() { }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleInitials { get; set; }
        public string Salutation { get; set; }
        public string Title { get; set; }
    }	
}