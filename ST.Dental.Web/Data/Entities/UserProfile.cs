namespace ST.Dental.Web.Data.Entities
{
    public class UserProfile
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string WebsiteUrl { get; set; }
        public string Bio { get; set; }
    }
}