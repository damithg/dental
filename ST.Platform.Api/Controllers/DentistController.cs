﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Remotion.Linq.Utilities;

namespace ST.Platform.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/dentist")]
    public class DentistController : Controller
    {
        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet("patient/{patientId}/mandate")]
        public string PatientMandate(int patientId)
        {
            return "";
        }

        [HttpPost("patient/{patientId}/sendmail/{emailTemplateId}")]
        public void SendPatientEmails(Template emailTemplateId, int patientId)
        {
        }

        [HttpPost("patient/{patientId}/passwordreset")]
        public void ResetPasswordEmail([FromBody]string patientId)
        {
        }

        //[HttpGet]
        //public void SendPortalDPASQuestionCSA(bool includeHeaderFooter, int sourcePracticeId, string freeText,
        //    string actorName)
        //{
        //}

        //[HttpGet]
        //public bool ValidateKYC(int pPatientID, string pAccountSortCode, string pAccountNumber,
        //    string pAccountFirstName, string pAccountLastName, string pActorName)
        //{
        //    return true;
        //}

        //[HttpGet]
        //public bool ValidateAccountCheckDigit(string pSortCode, string pAccountNumber)
        //{
        //    return true;
        //}

        //[HttpGet("api/bank/{bankIdentifier}/account/{accountIdentifier}")]
        //public bool ValidateBankVal(string bankIdentifier, string accountIdentifier)
        //{
        //    return true;
        //}
        //[HttpGet]
        //public string Ping()
        //{
        //    return $"Server time {0}, DateTime.UtcNow";
        //}


        //IList<Category> GetActiveCategories(int dentistId);

        //IList<Dentist> GetByFeeReviewDate(DateTime feeReviewDate);

        //IList<Dentist> GetByPractice(int practiceId);

        //Dentist GetByReference(int dentistRef);

        //IList<Int32> GetDentistRefsForReports();

        //void ChangeCategories(int categoryFrom, int categoryTo, string effectiveDate);

        //IList<Dentist> Search(string firstName, string lastName, string dentistRef,
        //    int practiceRef, int maximumRows, int startRowIndex);

        //int SearchResultCount(string firstName, string lastName, string dentistRef,
        //    int practiceRef);
    }

    public class Template
    {
    }
}