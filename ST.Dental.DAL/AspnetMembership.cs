﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class AspnetMembership : BusinessObject<Guid>
    {
        private AspnetUser _aspnetUser;

        private Guid _applicationId;
        private string _password;
        private int _passwordFormat;
        private string _passwordSalt;
        private string _mobilePIN;
        private string _email;
        private string _loweredEmail;
        private string _passwordQuestion;
        private string _passwordAnswer;
        private bool _isApproved;
        private bool _isLockedOut;
        private DateTime _createDate;
        private DateTime _lastLoginDate;
        private DateTime _lastPasswordChangedDate;
        private DateTime _lastLockoutDate;
        private int _failedPasswordAttemptCount;
        private DateTime _failedPasswordAttemptWindowStart;
        private int _failedPasswordAnswerAttemptCount;
        private DateTime _failedPasswordAnswerAttemptWindowStart;
        private string _comment;
        private bool _mustChangePassword;

        public AspnetMembership() { }

        #region Properties

        public virtual AspnetUser aspnetUser
        {
            get { return this._aspnetUser; }
            protected set { this._aspnetUser = value; }
        }

        protected virtual Guid applicationId
        {
            get { return this._applicationId; }
            set { this._applicationId = value; }
        }

        public virtual string password
        {
            get { return this._password; }
            set { this._password = value; }
        }

        public virtual int passwordFormat
        {
            get { return this._passwordFormat; }
            set { this._passwordFormat = value; }
        }
        public virtual string passwordSalt
        {
            get { return this._passwordSalt; }
            set { this._passwordSalt = value; }
        }

        public virtual string mobilePIN
        {
            get { return this._mobilePIN; }
            set { this._mobilePIN = value; }
        }

        public virtual string email
        {
            get { return this._email; }
            set { this._email = value; }
        }

        public virtual string loweredEmail
        {
            get { return this._loweredEmail; }
            set { this._loweredEmail = value; }
        }

        public virtual string passwordQuestion
        {
            get { return this._passwordQuestion; }
            set { this._passwordQuestion = value; }
        }

        public virtual string passwordAnswer
        {
            get { return this._passwordAnswer; }
            set { this._passwordAnswer = value; }
        }

        public virtual bool isApproved
        {
            get { return this._isApproved; }
            set { this._isApproved = value; }
        }

        public virtual bool isLockedOut
        {
            get { return this._isLockedOut; }
            set { this._isLockedOut = value; }
        }

        public virtual DateTime createDate
        {
            get { return this._createDate; }
            set { this._createDate = value; }
        }

        public virtual DateTime lastLoginDate
        {
            get { return this._lastLoginDate; }
            set { this._lastLoginDate = value; }
        }

        public virtual DateTime lastPasswordChangedDate
        {
            get { return this._lastPasswordChangedDate; }
            set { this._lastPasswordChangedDate = value; }
        }

        public virtual DateTime lastLockoutDate
        {
            get { return this._lastLockoutDate; }
            set { this._lastLockoutDate = value; }
        }

        public virtual int failedPasswordAttemptCount
        {
            get { return this._failedPasswordAttemptCount; }
            set { this._failedPasswordAttemptCount = value; }
        }

        public virtual DateTime failedPasswordAttemptWindowStart
        {
            get { return this._failedPasswordAttemptWindowStart; }
            set { this._failedPasswordAttemptWindowStart = value; }
        }

        public virtual int failedPasswordAnswerAttemptCount
        {
            get { return this._failedPasswordAnswerAttemptCount; }
            set { this._failedPasswordAnswerAttemptCount = value; }
        }

        public virtual DateTime failedPasswordAnswerAttemptWindowStart
        {
            get { return this._failedPasswordAnswerAttemptWindowStart; }
            set { this._failedPasswordAnswerAttemptWindowStart = value; }
        }

        public virtual string comment
        {
            get { return this._comment; }
            set { this._comment = value; }
        }

        public virtual bool mustChangePassword
        {
            get { return this._mustChangePassword; }
            set { this._mustChangePassword = value; }
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    password + "|" +
                    email + "|" +
                    createDate.ToString() + "|" +
                    lastLoginDate.ToString() + "|" +
                    lastLockoutDate.ToString() ).GetHashCode();
        }
		
    }
}
