﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Category_PriceHistory : BusinessObject<CompositeKey>
    {
        private Category _category;
        private System.Nullable<decimal> _fee;
        private System.Nullable<System.DateTime> _liveFrom;
        private System.Nullable<System.DateTime> _liveUntil;

        public Category_PriceHistory(){ }

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        #region Properties

        public virtual Category category
        {
            get { return this._category; }
            set { this._category = value; }
        }

        public virtual System.Nullable<decimal> fee
        {
            get{return this._fee;}
            set{this._fee = value;}
        }

        public virtual System.Nullable<System.DateTime> liveFrom
        {
            get{return this._liveFrom;}
            set{this._liveFrom = value;}
        }

        public virtual System.Nullable<System.DateTime> liveUntil
        {
            get{return this._liveUntil;}
            set{this._liveUntil = value;}
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    fee.ToString() + "|" +
                    liveFrom.ToString() + "|" +
                    liveUntil.ToString()).GetHashCode();
        }
    }	
}
