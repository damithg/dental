﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class UserSession : BusinessObject<string>
    {
        private AspnetUser _user;
        private DateTime _lastAccessTime;

        public UserSession() { }

        public virtual AspnetUser user
        {
            get { return this._user; }
            set { this._user = value; }
        }

        public virtual DateTime lastAccessTime
        {
            get { return this._lastAccessTime; }
            set { this._lastAccessTime = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    user.GetHashCode() + "|" +
                    lastAccessTime.ToString()).GetHashCode();
        }
    }	
}
