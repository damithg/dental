﻿using System;
using System.Collections.Generic;
using log4net;

namespace Quintus.Biz
{
    [Serializable]
    public class PrintJob : BusinessObject<int>
    {
        // Define the Log4Net logger.
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private DateTime? _startTime;
        private DateTime? _endTime;
        private string _jobName;                
        private AspnetUser _requestedBy;
        private IList<PrintRequest> _printRequest;
        private string _selectedPrinter;
        private int _requestedCount;
        private int _printedCount;
        private int _erroredCount;
        private int _successCount;
        private int _pendingCount;
        private bool _cancel;

        public virtual int pendingCount
        {
            get { return _pendingCount; }
            set { _pendingCount = value; }
        }
        
        public virtual int requestedCount
        {
            get { return _requestedCount; }
            set { _requestedCount = value; }
        }

        public virtual int printedCount
        {
            get { return _printedCount; }
            set { _printedCount = value; }
        }

        public virtual int erroredCount
        {
            get { return _erroredCount; }
            set { _erroredCount = value; }
        }

        public virtual int successCount
        {
            get { return _successCount; }
            set { _successCount = value; }
        }

        public PrintJob() { }

        public PrintJob(int Id, DateTime? startTime, DateTime? endTime, string jobName, int requestCount, int printedCount, int errorCount, int successCount, int pendingCount)
        {
            ID = Id;
            _startTime = startTime;
            _endTime = endTime;
            _jobName = jobName;
            _requestedCount = requestCount;
            _printedCount = printedCount;
            _erroredCount = errorCount;
            _successCount = successCount;
            _pendingCount = pendingCount;
        }

        public virtual string selectedPrinter
        {
            get { return _selectedPrinter; }
            set { _selectedPrinter = value; }
        }

        public virtual System.Nullable<System.DateTime> startTime
        {
            get { return this._startTime; }
            set { this._startTime = value; }
        }

        public virtual System.Nullable<System.DateTime> endTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        public virtual string jobName
        {
            get { return _jobName; }
            set { _jobName = value; }
        }

        public virtual AspnetUser requestedBy
        {
            get { return _requestedBy; }
            set { _requestedBy = value; }
        }

        public virtual IList<PrintRequest> printRequest
        {
            get { return _printRequest; }
            set { _printRequest = value; }
        }

        public virtual void Cancel()
        {
            _cancel = true;
        }

        public virtual bool isCancelled()
        {
            return _cancel;
        }
        //TODO: implement Timespan to indicate time spent on each job
        //public virtual TimeSpan duration
        //{
        //    get
        //    {
        //        if (endTime == DateTime.MinValue ||
        //            startTime == DateTime.MinValue ||
        //            startTime >= endTime)
        //        {
        //            return new TimeSpan(0);
        //        }
        //        else
        //        {
        //            return endTime.Subtract(startTime);
        //        }
        //    }
        //}
        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    startTime.ToString() + "|" +
                    endTime.ToString()).GetHashCode();
        }
    }	
}
