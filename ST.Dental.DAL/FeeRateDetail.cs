﻿using System;

namespace Quintus.Biz
{
    [Serializable]
    public class FeeRateDetail
    {
        private int _vid;
        private string _dentist;
        private string _categoryClass;
        private int _categoryId;
        private string _categoryName;
        private DateTime _startDate;
        private DateTime? _endDate;
        private decimal _fee;
        private int _discount;
        private int _extraDiscount;
        private decimal _monthlyFee;


        public FeeRateDetail(int Vid, string Dentist, string CategoryClass,
                int CategoryId, string CategoryName, DateTime StartDate,
                DateTime? EndDate, decimal Fee, int Discount, int ExtraDiscount,
                decimal MonthlyFee)
        {
            this._vid = Vid;
            this._dentist = Dentist;
            this._categoryClass = CategoryClass;
            this._categoryId = CategoryId;
            this._categoryName = CategoryName;
            this._startDate = StartDate;
            this._endDate = EndDate;
            this._fee = Fee;
            this._discount = Discount;
            this._extraDiscount = ExtraDiscount;
            this._monthlyFee = MonthlyFee;
        }

        public int vid { get { return _vid; } }
        public string dentist { get { return _dentist; } }
        public string categoryClass { get { return _categoryClass; } }
        public int categoryId { get { return _categoryId; } }
        public string categoryName { get { return _categoryName; } }
        
        public DateTime startDate 
        { 
            get { return _startDate; }
            set { _startDate = value; }
        }
        
        public DateTime? endDate 
        { 
            get { return _endDate; }
            set { _endDate = value; }
        }
        
        public decimal fee { get { return _fee; } }
        public int discount { get { return _discount; } }

        public int extraDiscount 
        { 
            get { return _extraDiscount; }
            set { _extraDiscount = value; }
        }

        public decimal monthlyFee { get { return _monthlyFee; } }
        public string description { get { return _categoryName + " - " + _fee.ToString("C"); } }
    }
}
