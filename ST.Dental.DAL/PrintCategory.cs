﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PrintCategory : BusinessObject<int>
    {
        private string _name;
        private int _requestCount;

        IList<PrintType> _printTypes;

        public PrintCategory(int Lid, string Name, int RequestCount)
        {
            this.ID = Lid;
            this.name = Name;
		    this.requestCount=RequestCount;
        }

        public PrintCategory() { }

        public virtual IList<PrintType> printTypes
        {
            get { return this._printTypes; }
            set { this._printTypes = value; }
        }

        public virtual string name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        public virtual int requestCount
        {
            get { return this._requestCount; }
            set { this._requestCount = value; }
        }


        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    name + "|" +
                    requestCount).GetHashCode();
        }
    }	
}
