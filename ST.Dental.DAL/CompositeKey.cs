﻿using System;

namespace Quintus.Biz
{
    [Serializable]
    public class CompositeKey
    {
        private int _lid;
        private int _vid;

        public virtual int lid
        {
            get { return this._lid; }
            set { this._lid = value; }
        }

        public virtual int vid
        {
            get { return this._vid; }
            set { this._vid = value; }
        }

        public CompositeKey() { }

        public CompositeKey(int Lid, int Vid) 
        {
            this.lid = Lid;
            this.vid = Vid;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (obj.GetType() != this.GetType()) return false;

            CompositeKey compareTo = obj as CompositeKey;

            return (compareTo != null) &&
                   (compareTo.lid == this.lid) &&
                   (compareTo.vid == this.vid);
        }

        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    _lid.ToString() + "|" +
                    _vid.ToString()).GetHashCode();
        }
    }
}
