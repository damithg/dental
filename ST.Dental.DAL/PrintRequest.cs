﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PrintRequest : BusinessObject<int>
    {
        private string _description;
        private int _quantity;
        private DateTime _dateRequested;
	    private System.Nullable<DateTime> _dateActioned;
        private string _status;
        private string _officeContact;
        private int _practiceRef;
        private string _templatepath;         

        private PrintTemplate _printTemplate;
        private PrintType _printType;
        private PrintJob _printJob;
        private IList<PrintParameter> _printParameters;

        public PrintRequest() { }

        #region Properties

            public virtual PrintTemplate printTemplate
            {
                get { return this._printTemplate; }
                set { this._printTemplate = value; }
            }

            public virtual PrintType printType
            {
                get { return this._printType; }
                set { this._printType = value; }
            }

            public virtual PrintJob printJob
            {
                get { return this._printJob; }
                set { this._printJob = value; }
            }

            public virtual IList<PrintParameter> printParameters
            {
                get 
                {
                    if (this._printParameters == null)
                        _printParameters = new List<PrintParameter>();
                    return this._printParameters; 
                }
                set { this._printParameters = value; }
            }

            public virtual string description
            {
                get { return _description; }
                set { _description = value; }
            }

            public virtual int quantity
            {
                get { return _quantity; }
                set { _quantity = value; }
            }

            public virtual DateTime dateRequested
            {
                get { return _dateRequested; }
                set { _dateRequested = value; }
            }

            public virtual DateTime? dateActioned
            {
                get { return _dateActioned; }
                set { _dateActioned = value; }
            }

            public virtual string status
            {
                get { return _status; }
                set { _status = value; }
            }

            public virtual string officeContact
            {
                get { return _officeContact; }
                set { _officeContact = value; }
            }

            public virtual int practiceRef
            {
                get { return _practiceRef; }
                set { _practiceRef = value; }
            }

            public virtual string templatepath
            {
                get { return this._templatepath; }
                set { this._templatepath = value; }
            }

        #endregion

        public virtual PrintParameter AddParameter(string Name, string Value)
        {
            PrintParameter param = new PrintParameter();
            param.name = Name;
            param.paramValue = Value;
            param.printRequest = this;

            printParameters.Add(param);

            return param;
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    description + "|" +
                    quantity.ToString() + "|" +
                    dateRequested.ToShortDateString() + "|" +
                    status + "|" +
                    officeContact + "|" +
                    practiceRef.ToString() ).GetHashCode();
        }
    }	
}
