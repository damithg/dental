﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class Dentist_PriceHistory : BusinessObject<CompositeKey>, ICheckTransient
    {
        private Dentist _dentist;
        private System.Nullable<decimal> _fee;
        private System.Nullable<System.DateTime> _liveFrom;
        private System.Nullable<System.DateTime> _liveUntil;

        public Dentist_PriceHistory() { }

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        #region ICheckTransient implementation

        private Boolean _isTransient = true;

        public virtual Boolean isTransient
        {
            get { return this._isTransient; }
            set { this._isTransient = value; }
        }

        #endregion

        #region Properties

        public virtual Dentist dentist
        {
            get { return this._dentist; }
            set { this._dentist = value; }
        }
        public virtual System.Nullable<decimal> fee
        {
            get{return this._fee;}
            set{this._fee = value;}
        }
        public virtual System.Nullable<System.DateTime> liveFrom
        {
            get{return this._liveFrom;}
            set{this._liveFrom = value;}
        }
        public virtual System.Nullable<System.DateTime> liveUntil
        {
            get{return this._liveUntil;}
            set{this._liveUntil = value;}
        }

        #endregion

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    fee.ToString() + "|" +
                    liveFrom.ToString() + "|" +
                    liveUntil.ToString()).GetHashCode();
        }


    }	
}
