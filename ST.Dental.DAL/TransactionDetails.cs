﻿using System;

namespace Quintus.Biz
{
    /// <summary>
    /// Encapsulates the transaction details for a particular transaction.  Note that this  
    /// isn't a DomainObject, just a POCO value object.  Furthermore, since it's a value
    /// object, it's immutable after construction.
    /// </summary>
    public class TransactionDetails
    {
        private int _lid;
        private int _vid;
        private DateTime _transactionDate;
        private DateTime _effectiveDate;
        private string _narrative;
        private string _entryDescription;
        private DateTime _dateCleared;
        private string _debitOrCredit;
        private decimal _amount;
        private string _settlementMethod;
        private string _accountName;


        public TransactionDetails(int Lid, int Vid, DateTime TransactionDate, 
            DateTime EffectiveDate, string Narrative, string EntryDescription, 
            DateTime DateCleared, string DebitOrCredit, decimal Amount, 
            string SettlementMethod, string AccountName)
        {
            this._lid = Lid;
            this._vid = Vid;
            this._transactionDate = TransactionDate;
            this._effectiveDate = EffectiveDate;
            this._narrative = Narrative;
            this._entryDescription = EntryDescription;
            this._dateCleared = DateCleared;
            this._debitOrCredit = DebitOrCredit;
            this._amount = Amount;
            this._settlementMethod = SettlementMethod;
            this._accountName = AccountName;
        }

        public int lid { get { return _lid; } }
        public int vid { get { return _vid; } }
        public DateTime transactionDate { get { return _transactionDate; } }
        public DateTime effectiveDate { get { return _effectiveDate; } }
        public string narrative { get { return _narrative; } }
        public string entryDescription { get { return _entryDescription; } }
        public DateTime dateCleared { get { return _dateCleared; } }
        public string debitOrCredit { get { return _debitOrCredit; } }
        public decimal amount { get { return _amount; } }
        public string settlementMethod { get { return _settlementMethod; } }
        public string accountName { get { return _accountName; } }
    }
}
