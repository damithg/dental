﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    [Serializable]
    public class LedgerTransactionEntries : BusinessObject<CompositeKey>
    {
        private LedgerTransaction _ledgerTransaction;
        private Category _category;
        private CategoryClass _categoryClass;

        private System.Nullable<int> _account;
        private System.Nullable<decimal> _amount;
        private System.Nullable<int> _costCenter;
        private System.Nullable<int> _currency;
        private System.Nullable<decimal> _currencyAmount;
        private System.Nullable<System.DateTime> _dateCleared;
        private System.Nullable<int> _debitOrCredit;
        private string _description;
        private System.Nullable<int> _settlementMethod;

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        public LedgerTransactionEntries(){}

        public virtual LedgerTransaction ledgerTransaction
        {
            get { return this._ledgerTransaction; }
            set { this._ledgerTransaction = value; }
        }

        public virtual Category category
        {
            get { return this._category; }
            set { this._category = value; }
        }

        public virtual CategoryClass categoryClass
        {
            get { return this._categoryClass; }
            set { this._categoryClass = value; }
        }

        public virtual System.Nullable<int> account
        {
            get{return this._account;}
            set{this._account = value;}
        }

        public virtual System.Nullable<decimal> amount
        {
            get{return this._amount;}
            set{this._amount = value;}
        }

        public virtual System.Nullable<int> costCenter
        {
            get{return this._costCenter;}
            set{this._costCenter = value;}
        }

        public virtual System.Nullable<int> currency
        {
            get{return this._currency;}
            set{this._currency = value;}
        }

        public virtual System.Nullable<decimal> currencyAmount
        {
            get{return this._currencyAmount;}
            set{this._currencyAmount = value;}
        }

        public virtual System.Nullable<System.DateTime> dateCleared
        {
            get{return this._dateCleared;}
            set{this._dateCleared = value;}
        }

        public virtual System.Nullable<int> debitOrCredit
        {
            get{return this._debitOrCredit;}
            set{this._debitOrCredit = value;}
        }

        public virtual string description
        {
            get{return this._description;}
            set{this._description = value;}
        }

        public virtual System.Nullable<int> settlementMethod
        {
            get{return this._settlementMethod;}
            set{this._settlementMethod = value;}
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    account.ToString() + "|" +
                    description).GetHashCode();
        }
    }	
}
