﻿using System;
using System.Collections.Generic;

namespace Quintus.Biz
{
    public class PrintFilter
    {
        private string _value;
        private Int64 _requestCount;

        public PrintFilter(string Value, Int64 RequestCount) 
        {
            this._value = Value;
            this._requestCount = RequestCount;
        }

        public PrintFilter(Int32 Value, Int64 RequestCount)
        {
            this._value = Value.ToString();
            this._requestCount = RequestCount;
        }

        public virtual string value
        {
            get { return this._value; }
            set { this._value = value; }
        }

        public virtual Int64 requestCount
        {
            get { return this._requestCount; }
            set { this._requestCount = value; }
        }
    }	
}
