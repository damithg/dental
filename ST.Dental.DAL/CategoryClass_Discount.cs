﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Quintus.Biz
{
    [Serializable]
    public class CategoryClass_Discount : BusinessObject<CompositeKey>
    {
        private CategoryClass _categoryClass;
        private System.Nullable<int> _percentageDiscount;
        private System.Nullable<int> _threshold;
        
        public CategoryClass_Discount(){}

        #region Internet Toolkit Fields
        protected int arrayIndex = 1;
        protected int pvid = 0;
        #endregion

        public virtual CategoryClass categoryClass
        {
            get { return _categoryClass; }
            set { _categoryClass = value; }
        }

        public virtual System.Nullable<int> percentageDiscount
        {
            get { return _percentageDiscount; }
            set { _percentageDiscount = value; }
        }

        public virtual System.Nullable<int> threshold
        {
            get { return _threshold; }
            set { _threshold = value; }
        }

        /// <summary>
        /// Hash code should ONLY contain the "business value signature" of the object and not the ID
        /// </summary>
        public override int GetHashCode()
        {
            return (GetType().FullName + "|" +
                    percentageDiscount.ToString() + "|" +
                    threshold.ToString()).GetHashCode();
        }
    }
}
