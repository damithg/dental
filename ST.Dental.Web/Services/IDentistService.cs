﻿using System.Collections;
using System.Collections.Generic;
using ST.Dental.Web.Data.Entities;

namespace ST.Dental.Web.Services
{
    public interface IDentistService
    {
        IEnumerable<Dentist> GetDentists(int practiceId);

    }
}